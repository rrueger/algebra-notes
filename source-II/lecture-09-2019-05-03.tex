\chapter{Constructibility}

\section{Some Examples}

\begin{example}[Non-counter example]
  Suppose we want to find a splitting field $E$ to some polynomial $p \in \F[Z]$
  such that $E/F$ is \textbf{not} a Galois extension. Naturally, $p$ cannot have
  distinct roots, otherwise the extension is normal and thus Galois. To find
  such an example we can look in $\F _p (X)$, the field of rational functions in
  $X$ with coefficients in $\F _p$. Let $E$ be the splitting field of $p(Z) =
  Z^p - X$. Suppose $r$ is a root of $p$, then we have $r^p -X = 0$ so $r^p = X$
  and then $p(Z) = Z^p - r^p = {(Z - r)}^p$ as we are in characteristic $p$.
  Clearly this polynomial has repeated roots. So we have that $E = F(r)$.
\end{example}

\begin{exercise}
  As contrived as it may seem, this is one of the simplest examples of a
  Non-Galois splitting field. Why?
\end{exercise}

\begin{example}[Infinite dimensional extension]
  We proved at the beginning of this semester, that for a set of $n$ pairwise
  distinct field automorphisms on some field $E$, which fix a field $H \subseteq
  E$ we have $\dim (E/H) \geq n$.  This gives an upper lower bound on the
  extension. Perhaps there are cases with $\dim (E/H) = \infty$. Infinite
  dimensional extensions haven't been the focus of our study yet of course they
  exist. We can choose as a base field $\F _p$; now clearly we can't choose $\F
  _{p^n}$ as an extended field, as $\dim (\F _{p^n} / \F _p) = n$, so instead we
  elect the algebraic closure $\overline{\F _p}$ which is infinite dimensional.
  We can now consider $\sigma _f (\alpha) = \alpha ^p$ a Frobenius automorphism
  and let $\set{\sigma}$ be the set of automorphisms in the theorem. Then the
  field fixed by $\set{\sigma}$ is the field with the property $X^p - X = 0$
  which is exactly $\F _p$.

  % @todo: Show that F_p is exactly the field that is fixed.
\end{example}


\section{Fundamental Examples of The Fundamental theorem of Galois Thoery}

We have seen that $\F _{p^n} / \F _p$ and $\C (X_1, \ldots, X_n) / \C (e_1,
\ldots, e_n)$ are Galois extensions. In this lecture we will investigate more
carefully the constructibility of $n$-gons, that is regular polygons with n
vertices.

We will now reason a series of reductions on what it means for a number to be
constructible prove one direction of Theorem~\ref{thm:gauss-wantzel}
(Gauss-Wantzel). We will show that if an $n$-gon is constructible, then $n$ is
the product of a power of two and a Fermat prime. So, we ask ourselves, how do
we construct the regular $n$-gon? Visually we have

\begin{center}
  \begin{tikzpicture}
    [
      scale=1,
      >=stealth,
      point/.style = {draw, circle,  fill = black, inner sep = 1pt},
      dot/.style   = {draw, circle,  fill = black, inner sep = 0.2pt},
    ]

    % The circle
    \def\rad{3}
    \node (origin) at (0,0) [point, label = {below right:$\mathcal{O}$}]{};
    \draw[dashed] (origin) circle (\rad);

    % The Axes
    \draw[->] (-1.25*\rad,0) -- (1.25*\rad,0);
    \draw[->] (0,-1.25*\rad) -- (0, 1.25*\rad);

    % Points
    \node (t1) at +(0:\rad)    [point] {};
    \node (t2) at +(120:\rad)  [point, label = above:$\omega _3$] {};
    \node (t3) at +(-120:\rad) [point] {};

    \node (s1) at +(0:\rad)   [point] {};
    \node (s2) at +(90:\rad)  [point, label = above right:$\omega _4$] {};
    \node (s3) at +(180:\rad) [point] {};
    \node (s4) at +(270:\rad) [point] {};

    \node (p1) at +(0:\rad)    [point] {};
    \node (p2) at +(72:\rad)   [point, label = above right:$\omega _5$] {};
    \node (p3) at +(144:\rad)  [point] {};
    \node (p4) at +(-144:\rad) [point] {};
    \node (p5) at +(-72:\rad)  [point] {};

    % Line connecting
    \draw [red] (t1) -- (t2) -- (t3) -- (t1);
    \draw [blue] (s1) -- (s2) -- (s3) -- (s4) -- (s1);
    \draw [green] (p1) -- (p2) -- (p3) -- (p4) -- (p5) -- (p1);
  \end{tikzpicture}
\end{center}
%
and the question becomes, for which $n$ can we construct $\omega _n = e^{\frac{2
\pi}{n} \ii}$? Again, we know that this is only constructible if $\omega _n \in
E$ for which $E$ has a complete tower of degree 2 extensions reaching down to
$\Q$. This is useful, as this is a completely algebraic description of our
problem. We are now only interested in the extension of $\Q (\omega _n) / \Q$.

\subsection{The extension $\Q (\omega _n) / \Q$}

Henceforth we'll fix $n \geq 2$ and drop the index $n$ to identify $\omega _n =
\omega$. We immediately see that $\omega$ is a root of the polynomial $X^n - 1$
meaning that it is an algebraic number. This is good as it allows us to apply
all of our known tools. In particular we note that if $\omega$ is constructible,
it lives in some field $E$ which is an extension of degree $2^k$ for some $k$
over $\Q$. In turn the extension $\Q (\omega) / \Q$ must then also have a degree
of a power of two.
% @todo: Why must it be a power of two?
% We know this as $\Q (\omega) \subseteq E$, and thus corresponds to some group
% $S \subset \Gal (E/ \Q (\omega))$.
%
% The problem here is that it uses that the extension is Galois. This is true,
% but it isn't proven yet.
In essence, we want to answer the two questions
\begin{enumerate}[(i)]
  \item What is $\dim (\Q (\omega) / \Q)$?
  \item Is $\Q (\omega) / \Q$ Galois?
\end{enumerate}

To answer (i), we will find the minimal irreducible polynomial with root
$\omega$ over $\Q$. It is then one of our results from field theory, that the
degree of this polynomial is the degree of the extension. To do this we will
take a short excursion concerning itself with the factorisation of $X^n - 1$. We
can look at examples for $n=1, 2, 3, 4$:
%
\begin{itemize}
  \item (n=2, $\omega = -1$) The equation $X^2 - 1$ factorises to $(X+1)(X-1)$.
  \item (n=3, $\omega = -\frac{1}{2} + \ii \frac{\sqrt{3}}{2}$) The equation
    $X^3 - 1$ factorises to $(X-1)(X^2 + X + 1)$.
  \item (n=4, $\omega = -\ii$) The equation $X^4 - 1$ factorises to
    $(X-1)(X+1)(X^2 + 1)$.
\end{itemize}
%
A general factorisation of $X^n - 1$ over $\C$ is $\prod _{j=1} ^{n-1} X -
\omega ^j$ and we know that $\Sigma := \set{\omega ^0, \omega ^1, \ldots, \omega
^{n-1}}$ forms a group. This last fact allows us to observe an innocent detail
stemming from group theory that for $g \in \Sigma$
%
\begin{align*}
  \mathrm{ord}(g) \mid (|\Sigma| = n)
\end{align*}
%
We can see how this manifests itself in an example
\begin{example}[n=4]
  Then $\Sigma = \set{1, \ii, -1, -\ii}$, and $\mathrm{ord}(1) = 1,
  \mathrm{ord}(\ii) = 4, \mathrm{ord}(-1) = 2, \mathrm{ord}(-\ii) = 4$. Looking
  at the factorisation $X^4 - 1 = (X + 1)(X - 1)(X^2 + 1)$ we see that there is
  a factor for each order.
\end{example}

This gives rise to the idea, that we could group $\Sigma$ by the order of each
element. For this we will introduce

\begin{definition}[Primitive root of unity]
  A \textbf{primitive} n\textsuperscript{th} \textbf{root} of unity, is one that
  generates all the n\textsuperscript{th} roots of unity. This means the
  primitive roots are exactly those of order $n$.
\end{definition}

\begin{definition}[n\textsuperscript{th} Cyclotomic Polynomial]
  Let $J \subseteq \Sigma$ consist of the primitive roots of unity, then we
  define the n\textsuperscript{th}
  \begin{align*}
    \Phi _n (X) = \prod _{z \in J} (X - z)
  \end{align*}
\end{definition}

\begin{example}
  For $n=4$, we have
  \begin{align*}
    \Phi _4 (X) = (X - \ii)(X + \ii) = X^2 + 1
  \end{align*}
\end{example}

\begin{theorem}
  \begin{align*}
    \Phi _n \in \Q [X]
  \end{align*}
\end{theorem}

\begin{proof}
  We claim that $\Q (\omega)$ is the splitting field of $p(X) = X^n - 1$. This
  holds, as $\Q (\omega)$ clearly contains all the roots of $p$ and it cannot
  split in a subfield as a subfield must contain $\omega$, yet $\Q (\omega)$ is
  by definition the smallest field containing $\Q$ and $\omega$.

  Now let $\sigma \in \Gal (\Q (\omega) / \Q)$.  Since $X^n - 1 = 0$ we have
  $X^n = 1$ and $\sigma (X^n) = {\sigma (X)} ^n = 1$. We can now assert that
  $\sigma$ preserves the order of a root. Assume this were not the case: then
  for a primitive root $z$ there would be some $k < n$ such that ${\sigma (z)}^k
  = 1 = {\sigma (z)}^n$. We would then have ${\sigma (z)} ^{n-k}= \sigma
  (z^{n-k}) = 1$. Applying the inverse of $\sigma$ gives $z ^{n-k} = 1$: a
  contradiction to $z$ being of order n i.e.\ primitive.

  In particular, this means that $\sigma$ maps primitive roots to primitive
  roots. We then draw
  %
  \begin{align*}
    \sigma (\Phi _n (X))
    = \sigma \left ( \prod _{z~\text{prim.}} (X - z) \right )
    = \prod _{z~\text{prim.}} (X - \sigma (z))
    = \prod _{z~\text{prim.}} (X - z)
    =  \Phi _n (X)
  \end{align*}
  %
  that $\Phi _n$ is Galois invariant. It follows that the coefficients are fixed
  by the Galois group, and thus must be rational. $\Phi _n \in \Q[X]$.
\end{proof}

This theorem delivers an immediate answer to question (ii)

\begin{corollary}[$\Q (\omega) / \Q$ is Galois]
  It is the splitting field of a polynomial in $\Q [X]$ with distinct roots,
  thus normal and hence Galois.
\end{corollary}

\begin{theorem}
  The n\textsuperscript{th} cyclotomic polynomial s irreducible over the
  rationals.
\end{theorem}

We will discuss the proof in a later lecture, and focus on what ramifications
this entails. Specifically, we will have the degree of the extension $\Q
(\omega) / \Q$, namely the degree of the n\textsuperscript{th} cyclotomic
polynomial.  The degree of $\Phi _n$ is given by the number of primitive roots.
We know $\set{\omega ^0, \omega ^1, \ldots, \omega ^{n-1}} \cong
\rquot{\Z}{n\Z}$, so the number of roots with order $n$ is equal to the number
of elements in $\rquot{\Z}{n\Z}$ with order n. The roots of order $n$ however
are exactly the primitive roots, and the elements in $\rquot{\Z}{n\Z}$ with
order $n$ are exactly those $\overline{k}$ with $k$ coprime to $n$. So we may
hunt the elements $k \in \set{1, \ldots, n}$ with $(n, k) = 1$. In general this
is difficult so we simply give this a name

\begin{definition}[Euler's totient function]\label{def:euler_totient}
  We define $\varphi (n){:}\, \N \to \N$ as the number of elements $1 \leq k \leq
  n$ that are coprime to $n$.
\end{definition}

\begin{lemma}
  For $p$ prime, it holds that
  \begin{align*}
    \varphi(p^n) = p^{n-1} (p-1)
  \end{align*}
\end{lemma}

\begin{proof}
  The total number of candidates $1 \leq k \leq p^n$ are $p^n$. The only ones
  that aren't coprime, are those that are multiples of $p$, of which there are
  $p^{n-1}$ in $\set{1, \ldots, p^n}$. Thus the total number that are coprime
  $p^n - p^{n-1} = p^{n-1}(p-1)$.
\end{proof}

\noindent
To conclude, we have

\begin{theorem}
  For $n \geq 2, \omega = e^{\frac{2\pi}{n}\ii}$,
  \begin{align*}
    \dim (\Q (\omega) / \Q) = \varphi(n)
  \end{align*}
\end{theorem}

\noindent
We can now return to the original problem of constructing $n$-gons.

\subsection{Construction}

We have now that if $\omega$ is constructible, then $\dim (\Q (\omega) / \Q) =
\varphi(n) = 2^k$ for some $k$. We can now factor $n = 2^a p_1 ^{\lambda _1}
\cdots p_n ^{\lambda _n}$ for $p_i$ distinct primes not equal 2 and as a
consequence of the Chinese remainder theorem, we have that
%
\begin{align*}
  \varphi (n)
  = \varphi (2^a) \varphi (p_1 ^{\lambda _1}) \cdots \varphi (p_n ^{\lambda _n})
  = 2^{a-1} p_1 ^{\lambda _1 -1} (p_1 -1) \cdots p_n ^{\lambda _n -1} (p_n -1).
\end{align*}
%
The only chance that $\varphi (n) = 2^k$ is if each $\lambda _j = 1$ and if each
$p_j - 1 = 2^{k_j}$ i.e.\ each $p_j = 2^{j_k} + 1$.

% @todo: Why is this then a Fermat prime?

In conclusion we have that the regular $n$-gon is constructible if $n$ is the
product of a power of $2$ and some Fermat primes.
The theorem of Gauss is one that goes in both directions, we will prove the
converse next lecture.
