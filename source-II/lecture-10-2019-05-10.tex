\chapter{The Cyclotomic Extension}

\section{Cyclotomic Polynomial}

We begin with an observation
%
\begin{align*}
  X^n -1 = \prod _{d | n} \Phi _d(X)
\end{align*}
%
\textit{Why should this be true?} Well, $X^n - 1$ contains all the $n$th roots
of unity, which form a group $\Gamma _n$. By Lagrange's theorem, the order $d$
of any root $\gamma _d \in \Gamma _n$ must divide the order of $\Gamma _n$,
namely $n$.
We conclude that every root $\gamma _d \in \Gamma _n$ appears on the R.H.S as a
root of $\Phi _d(X)$, and thus the R.H.S is merely a reordering of all the roots
of $X^n - 1$. We can use this observation to go on to prove

\begin{theorem}[Cyclotomic polynomial live in the world of integers]
  \begin{align*}
    \Phi _n(X) \in \Z [X]
  \end{align*}
\end{theorem}

Before we prove this statement, we remind of Gauss's Lemma from Algebra I.
\begin{siderules}
  \begin{remark}[Reminder, Algebra I - Gauss's Lemma]
    Let $R$ be a UFD, let $Q = \Frac(R)$, and let $f(x) \in R[x]$. If $f = GH$
    in $Q[x]$, then there is a factorization
    %
    \begin{align*}
      f = gh \text{~in~} R[x],
    \end{align*}
    %
    where $\deg(g) = \deg(G)$ and $\deg(h) = \deg(H)$; in fact, $G$ is a
    constant multiple of $g$ and $H$ is a constant multiple of $h$. Therefore,
    if $f$ does not factor into polynomials of smaller degree in $R[x]$, then
    $f$ is irreducible in $Q[x]$.
  \end{remark}
\end{siderules}

\begin{proof}[Proof by induction]
  For $n=1$, $\Phi _1(X) = X - 1 \in \Z[X]$. Now assume we have proven the
  assertion for $k < n$. So we can write
  %
  \begin{align*}
    X^n - 1
    = \prod _{d | n} \Phi _d(X)
    = \Phi _n \prod _{\substack{d | n \\ d \neq n}} \Phi _d(X)
    = \Phi _n \prod _{\substack{d | n \\ d < n}} \Phi _d(X)
  \end{align*}
  %
  and define
  %
  \begin{align*}
    A(X) = \prod _{\substack{d | n \\ d < n}} \Phi _d(X)
  \end{align*}
  %
  as each $\Phi _d(X) \in \Z [X]$ by assumption, we have that $A(X) \in \Z [X]$.
  So we have
  %
  \begin{align*}
    X^n - 1 = \Phi _n(X) A(X)
  \end{align*}
  %
  and note that $A(X)$ is monic. Completing the proof is now only an exercise in
  applying Gauss's lemma: \textit{A priori} $X^n - 1 = \Phi _n(X) A(X)$ is a
  factorisation taking place in $\Q = \Frac (\Z)$. Gauss gives us some other
  factorisation $X^n - 1 = F(X)G(X)$ with $F, G \in \Z [X]$ and $F = c \Phi
  _n(X)$ and $F = d A(X)$ for $c, d \in \Q$. This amounts to
  %
  \begin{align*}
    X^n - 1 = F(X) G(X) = cd \Phi _n(X) A(X) \implies cd = 1
  \end{align*}
  %
  as $A(X)$ and $\Phi _n(X)$ are monic. However, as $A(X)$ is monic, $d = 1$, so
  $c = 1$ and thus $F(X) = \Phi _n(X)
  \in \Z [X]$.
\end{proof}

\begin{theorem}
  $\Phi _n(x)$ is irreducible over $\Z$.
\end{theorem}

\begin{proof}[Proof by contradiction]
  Assume it \textit{is} reducible and remind that $\Phi _n(X)$ is the monic
  polynomial with roots exactly the primitive $n$th roots of unity. Then $\Phi
  _n(X) = B(X)C(X)$, with $B(X), C(X)$ non-constant and without loss of
  generality $B(X)$ is irreducible. If $B(X)$ is not irreducible, keep splitting
  the $B(X)$ until there is an irreducible factor. Now, let $\gamma$ be a root
  of $B(X)$. By definition, $\gamma$ is a root of $\Phi _n(X)$, hence a
  primitive root of unity. The strategy of this proof is to show that, in fact,
  $B(X)$ contains \textbf{all} primitive roots of unity, leaving none for $C(X)$
  and thus arriving at a contradiction. This strategy will be implemented by
  showing two facts for a prime $p$ coprime to n
  %
  \begin{enumerate}
    \item \textit{(Result 1)} $\gamma ^p$ is also a primitive root of unity
    \item \textit{(Result 2)} $\gamma ^p$ is also a root of $B(X)$
  \end{enumerate}
  %
  Once these have been shown, it follows that $B(X)$ contains \textbf{all}
  primitive roots of unity as the statements hold for all $p$ prime and coprime
  to $n$. This results in $C(X)$ merely being a constant. A direct contradiction
  to the hypothesis that $\Phi _n(X)$ can be factored into two non-constant
  polynomials.

  \begin{proof}[Proof Result 1]
    The fact that $n$ is coprime to $p$ delivers some $k,m$ such that $kp - mn =
    1$. Then
    %
    \begin{align*}
      \gamma ^{pk - nm} & = \gamma \\
      \implies
      \gamma ^{pk}
      & = \gamma \cdot \gamma ^{nm}
      = \gamma \cdot {(\gamma ^n)}^m
      = \gamma \cdot 1^m
      = \gamma
    \end{align*}
    If $\gamma ^p$ generates $\gamma$ which in turn generates $\Gamma
    _n$, then $\gamma ^p$ also generates $\Gamma _n$.
  \end{proof}
  %
  \begin{proof}[Proof Result 2]
    To show that this is the case we suppose by contradiction that $\gamma ^p$
    is a root of $C(X)$. Now, if $\gamma ^p$ is a root of $C(X)$, then $\gamma$
    is a root of $C(X^p)$ and, as before, a root $B(X)$. With $B(X)$ being
    irreducible and sharing a root with $C(X)$ we have that $B(X) | C(X^p)$. Now
    we perform a reduction of the coefficients by mod $p$.
    %
    \begin{align*}
      \Phi _n(X)                     & = B(X)C(X) \\
      \implies \overline{\Phi _n}(X) & = \overline{B}(X)\overline{C}(X)
    \end{align*}
    %
    Importantly, we still have that $\overline{B}(X) | \overline{C}(X^p)$ and
    notably we have that
    %
    \begin{align*}
      \overline{C}(X^p) = {\overline{C}(X)}^p.
    \end{align*}
    %
    so $\overline{B}(X) | {\overline{C}(X)}^p$. This means that they share
    non-trivial factors. Specifically, that they share roots. This leads to the
    observation, that in $\Z / p\Z [X]$,
    %
    \begin{align*}
      \overline{X^n -1}
      = X^n - \overline{1}
      = \overline{A}(X) \overline{\Phi _n}(X)
      = \overline{A}(X)\overline{B}(X)\overline{C}(X)
    \end{align*}
    %
    has a repeated root. This would imply that
    %
    \begin{align*}
      \gcd (X^n - \overline{1}, \overline{n}X^{n-1}) \neq 1
    \end{align*}
    %
    which, as $\overline{n} \neq 0$ ($p$ and $n$ are coprime), is nonsense.
    They clearly don't share a factor.
  \end{proof}
\end{proof}

\section{The Return of Gauss-Wantzel}

Previously, we had shown one direction of Theorem~\ref{thm:gauss-wantzel}. We
will now complete the proof. Namely, that the regular n-gon can be constructed
with a straightedge and compass only if $n$ is the product of a power of two and
a Fermat prime.

\begin{proof}
\end{proof}
