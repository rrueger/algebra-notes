\chapter{Galois Theory without Galois Extensions}

When dealing with Galois extensions, it at some point becomes inevitable to ask:
\textit{What do we do when an extension is \textbf{not} Galois?}. Will
\textit{none} of our tools work? It turns out, that when we consider some finite
extension $E/F$ not Galois, but $E$ is contained in some field $\hat{E}$ such
that $\hat{E}/F$ \textit{is} Galois we can apply some known results to arrive at
the

\begin{theorem}[Primitve Element Theorem]\label{thm:pet}
  Let $E/F$ be a finite extension, with F a field with characteristic 0. Then
  there exists some $\alpha$ in $E$ such that $E = F(\alpha)$.
\end{theorem}

However, before we prove this, we will take a step back and investigate in
which situations such an $\hat{E}$ exists.


\section{Reminder on Characteristics}

\begin{definition}[Reminder Algebra I, Characteristic]
  The characteristic, $p$, of a field is given by the number of times the
  multiplicative neutral element must be added to itself before we arrive at
  the additive neutral element. If no such $p$ exists, we say the field has
  characteristic 0.
  \begin{align*}
    \underset{p \text{~times}}{\underbrace{1+1+1+\cdots}} = 0
  \end{align*}
\end{definition}

\begin{example}~
  \begin{itemize}
    \item The characteristic of $\F _{p^n}$ is $p$.
    \item The characteristics of $\R, \C, \Q$ are $0$.
  \end{itemize}
\end{example}

This definition becomes more natural when considering it in the following
manner: let $\chi \colon \Z \to k$ be a homomorphism, mapping $n \mapsto nl$
for $l$ some unit of $k$. Then $\ker (\chi) = (p)$ for $p$ zero or a prime
(This is a result from Algebra I. It is a direct consequence of $\Z$ being a
PID and $k$ an integral domain). If $p \neq 0$, then $p$ is clearly the
characteristic as defined before, since if $x \in (p) = \ker (\chi)$, then $x
= pl$ as $(p)$ is a prime ideal. Then
%
\begin{align*}
   pl
   = 0
   = pll^{-1}
   = p1
   = \underset{p \text{~times}}{\underbrace{1+1+1+\cdots}}
\end{align*}
%
Assigning a field characteristic zero, when no such $p$ exists at first seemed
somewhat arbitrary, but now is quite canonical, as it simply is the $p$ that
generates the ideal that is $\ker (\chi)$.


\section{Characteristic 0}

\begin{theorem}\label{thm:existence_of_galois}
  Consider a finite field extension $E/F$ and suppose $F$ has characteristic 0.
  Then there exists some $\hat{E}$ that contains $E$ with $\hat{E}/F$ being
  Galois.
\end{theorem}

\begin{proof}
\end{proof}

%\begin{proof}
%  We will utilise the splitting field of a polynomial with distinct roots in
%  this proof as by the characterisation of Galois extensions over normal
%  extensions we will have our $\hat{E}$. So we must seek out a polynomial $P \in
%  F[X]$ with distinct roots whose splitting field will contain $E$. The
%  extension is finite, so we can write $E = F(\alpha _1, \ldots, \alpha _m)$. It
%  seems then close at hand then to look for a polynomial with roots $\alpha _1,
%  \ldots, \alpha _m$. We already know of some polynomials with some $\alpha _i$
%  as roots, namely the irreducible minimal polynomials of each $\alpha _i$, $p_i
%  \in F[X]$ - the existence of these is guaranteed by the fact that our
%  extension is finite, and thus algebraic. We can guarantee that $P$ has
%  \textit{all} the roots $\alpha_ i$ if we define
%  \begin{align*}
%    \quad \quad \quad \quad
%    P = \prod _{i=1} ^m p_i \quad \in F[X]
%  \end{align*}
%  However, we have no idea whether $P$ has distinct roots. In fact, we know that
%  in general it will not. We may have to omit some $p_i$'s for any hope for this
%  to work. Even then, it may be that a $p_i$ has repeated roots. So let us
%  investigate the $p_i$'s.

%  We will show that each $p_i$ has distinct roots, by showing that
%  %
%  \begin{align*}
%    \gcd (p_i, p_i ') = 1
%  \end{align*}
%  %
%  Immediately we see that $\deg (p_i) \geq 2$, otherwise $\alpha_i$ would lie in
%  $F$.
%  Crucially, also, we have that $p_i ' \neq 0$ as if $p_i = X^d + \cdots$, then
%  $p_i ' = d X^{d-1} + \cdots$ and in characteristic 0, we have guaranteed that
%  $d \neq 0$ and thus $p' \neq 0$.  We now suppose by contradiction, that $\gcd
%  (p_i, p_i') \neq 1$. It then follows that $p_i ' | p_i$ - a contradiction to
%  $p_i$ being irreducible. We are closer to our goal. The $p_i$'s have distinct
%  roots.  What about $p_i$ and $p_j$? We know they can't share a subset of their
%  roots, as otherwise, by previous argumentation, some factor would divide them.
%  However, they could be the same. If so omit one of them in the product.
%\end{proof}

\section{Characteristic p}

In the case of $F$ having characteristic $p \neq 0$, there is no such $\hat E$
in general. To show this, we pose the following counterexample:

Let $F = \F _p(X)$. Moreover, let $E = F(r)$ for $r$ a root of $p(Z) = Z^p - X$.
We immediately now notice that $p'(Z) = 0$, so our previous line of argument
doesn't work. Since $r$ is a root, $0 = p(r) = r^p - X$ implying $X = r^p$ so
$P(Z) = Z^p - r^p$; as we are in characteristic $p$ we have that $p(Z) = Z^p -
r^p = {(Z - r)}^p$. This means that $E$ is the splitting field over a polynomial
with a single repeated root. We know that the automorphism group of $E/F$ can
only permute the roots, and as there is only one root, $\Aut (E/F)$ is trivial.
The extension is not Galois. The question is now, \textit{is there are larger
$\hat E \supseteq E$ such that $\hat E /F$ is Galois?} Assume there was such an
$\hat E$, then for any automorphism $\sigma \in \Aut (\hat E /F)$ it holds that
$X \in F = \F _p (X)$ is fixed, so is $X^p$ and thus also $r$. It follows that
$F(r) = E$ is also fixed by $\sigma$ and thus not \textbf{only} $F$ is fixed,
meaning the extension is not Galois by the very first definition of a Galois
extension.


\section{Primitive element theorem}

\noindent
We are now in a position to prove the primitive element theorem~\ref{thm:pet}.

\begin{proof}
  To that end, let $E/F$ be a finite extension $\Char (F) = 0$.  As the
  extension is finite, we assume $E = F(\alpha, \beta)$. Cases in which more
  generators are needed are them proven by induction. From
  Theorem~\ref{thm:existence_of_galois}, we have some $\hat E$ with $E \subseteq
  \hat E$ and $\hat E /F$ Galois. By the Galois correspondence there can only be
  finitely many subextensions $\hat E / K / F$, hence only finitely many
  subextensions $E / K /F$. We can now consider the extensions over the
  subfields
  %
  \begin{figure}[H]
    \centering
    \begin{tikzpicture}
      \matrix(m)[%
        matrix of math nodes,
        row sep=5em,
        column sep=3em,
        minimum width=1em,
      ]{%
                          &                    & E      &                     &        \\
        F(\alpha + \beta) & F(\alpha + 2\beta) & \cdots & F(\alpha + k \beta) & \cdots \\
                          &                    & F      &                     &        \\
      };
      \path[-stealth]
        (m-2-1) edge [-] node [above] {$\quad\cong$} (m-2-2)
        (m-2-2) edge [-] node [above] {$\cong$} (m-2-3)
        (m-2-3) edge [-] node [above] {$\cong$} (m-2-4)
        (m-2-4) edge [-] node [above] {$\cong\quad$} (m-2-5)

        (m-3-3) edge [-] (m-2-1)
        (m-3-3) edge [-] (m-2-2)
        (m-3-3) edge [-] (m-2-3)
        (m-3-3) edge [-] (m-2-4)
        (m-3-3) edge [-] (m-2-5)

        (m-1-3) edge [-] (m-2-1)
        (m-1-3) edge [-] (m-2-2)
        (m-1-3) edge [-] (m-2-3)
        (m-1-3) edge [-] (m-2-4)
        (m-1-3) edge [-] (m-2-5)
        ;
    \end{tikzpicture}
  \end{figure}
  %
  \noindent
  for every $k \in \Z$. This generates infinitely many extensions, so they can't
  all be distinct. There is some $F(\alpha + m \beta) = F(\alpha + k\beta)$. So
  $\alpha + m\beta, \alpha + k\beta \in F(\alpha + m\beta)$, and thus their
  difference must be too $(m-k) \beta \in F(\alpha + m\beta)$. As $F$ has
  characteristic $0$, the prime field is isomorphic to $\Q$, and thus $(m-k) \in
  F$. We can conclude $\beta \in F(\alpha + m\beta)$ and thus $\alpha \in
  F(\alpha + m\beta)$. Finally, $E = F(\alpha, \beta) = F(\alpha + m \beta)$.
\end{proof}

\section{Skew Fields and Wedderburn's Theorem}

\begin{definition}[Skew Field, Division Algebra]
  A \textbf{skew field}, or \textbf{division algebra} is \textit{almost} a
  field. A skew field is defined like a field, with the exception that the
  multiplicative group must not be abelian. Formally, there is an abelian group
  $(A, +, 0)$, a multiplicative group $(A, \cdot, 1)$ and the same compatibility
  laws as in the definition of a field.
\end{definition}

\begin{theorem}[Wedderburn]\label{thm:weddeburn}
  Finite skew fields are fields.
\end{theorem}

\begin{remark}
  Here the real utility of the definition of a skew field becomes apparent: one
  can show that a finite structure is a field without having to show
  commutativity of multiplication. It reminds of a similar result that
  we have seen in Algebra I, namely that finite domains are fields. Once we have
  proven Theorem~\ref{thm:weddeburn}, we will have shown that there is no
  distinction between finite fields, finite domains and finite skew fields.
\end{remark}

\begin{lemma}\label{lemma:stab_central}
  When the multiplicative group of  field acts on itself by conjugation it holds
  that
  %
  \begin{align*}
    Z(x) = \Stab _{F^\times} (x) \sqcup \set{0}
  \end{align*}
  %
  where $Z(x) = \set{y \in F : xy = yx}$ denotes the centraliser of $x$ in $F$,
  the field.
\end{lemma}

\begin{proof}
  By definition of a field $F = F^\times \sqcup \set{0}$. Then
  \begin{align*}
    Z(x)
    & = \set{y \in F: xy = yx}                          \\
    & = \set{y \in F^\times: xy = yx} \cup \set{0}      \\
    & = \set{y \in F^\times: x = yxy^{-1}} \cup \set{0} \\
    & = \Stab _{F^\times} (x) \cup \set{0}
  \end{align*}
\end{proof}

\begin{proof}[Proof of Theorem~\ref{thm:weddeburn}]
  To show this, it only remains to show that all elements commute. To that end
  let $A$ be a skew field, and $F = Z(A) = \set{x \in A : \forall a \in A: ax =
  xa}$ its centre. We immediately note that $F$ is a field, and as it is finite
  (as a subset of a finite skew field), it is isomorphic to some $\F _q$. So $A$
  is some $F$ vector space with dimension $n$ meaning $|A| = |F|^n = q^{n}$.
  Our goal is to show that $n=1$ and thus $A = F$.

  We will show this by contradiction, but first, we note some numerical results:
  As we have done many times before, we'll look at the action of $A^\times$ on
  itself, acting by conjugation. As orbits form a partition, we immediately see
  that
  %
  \begin{align*}
    |A^\times|
    = |A| - 1
    = q^n -1
    = \sum _{\substack{x \\ \text{~repr.}}} |\Orb _{A^\times} (x)|
  \end{align*}
  %
  In fact, we know that all elements in $F$ have a trivial conjugacy class, so
  we can further write
  %
  \begin{align*}
    q^n - 1
    = |F| + \sum _{\substack{x \notin F \\ \text{~repr.}}} |\Orb _{A^\times}(x)|
    = q-1 + \sum _{\substack{x \notin F \\ \text{~repr.}}} |\Orb _{A^\times}(x)|
  \end{align*}
  %
  ``$x \notin F \text{~repr.}$'' being representatives of orbits not contained
  in $F$. This is well defined, as if any element of a conjugacy class lies in
  $F$, then that conjugacy class is trivial. Using the orbit-stabiliser theorem,
  we know that each
  %
  \begin{align*}
    |\Orb _{A^\times} (x)|
    = \frac{|A^\times|}{|\Stab _{A^\times} (x)|}
    = \frac{q^n -1}{|\Stab _{A^\times} (x)|}.
  \end{align*}
  %
  We can now investigate the stabilisers $\Stab _{A^\times} (x)$. From
  Lemma~\ref{lemma:stab_central}, we see that working with $Z(x)$ gives us
  almost identical results, but being a field makes it much easier it much
  easier to do that work. In fact, on inspection, we see that $F \subset Z(x)$.
  This inclusion is clear, when framed slightly differently: $Z(x)$ is the field
  of elements that commute with $x$, whereas $F$ is the field of elements that
  commute with \textit{all} $a \in A$. Specifically, we now have that $F
  \subseteq Z(x) \subseteq A$, so $Z(x)$ is a vector space over $F$, with its
  dimension $m_x$ a divisor of $n$. We can now write
  %
  \begin{align*}
    |\Orb _{A^\times} (x)|
    = \frac{q^n -1}{|\Stab _{A^\times} (x)|}
    = \frac{q^n -1}{|Z(x) \setminus \set{0}|}
    = \frac{q^n -1}{q^{m_x} - 1}
  \end{align*}
  %
  and in summary we now have
  %
  \begin{align}\label{eq:main}
    q^n - 1
    = q - 1 + \sum _{x \notin F \text{~repr.}} \frac{q^n -1}{q^{m_x} - 1}.
  \end{align}
  %
  Hearkening back to our work with cyclotomic polynomials, it seems sensible to
  consider this as a polynomial in $\Z[q]$, and write
  %
  \begin{align*}
    q^n - 1 = \prod _{d | n} \Phi _d (q), \quad
    q^{m_x} - 1 = \prod _{d | m_x} \Phi _{d} (q)
  \end{align*}
  %
  Since each $m_x$ divides $n$, each $d$ that divides $m_x$ also divides $n$;
  consequently
  %
  \begin{align*}
   \frac{q^n - 1}{q^{m_x} - 1} \in \Z [q].
  \end{align*}
  %
  still contains all primitive roots. In turn, we then have
  %
  \begin{align*}
    \Phi _n (q) \,\, | \,\, \left ( \frac{q^n - 1}{q^{m_x} - 1} \right )
  \end{align*}
  %
  and, by rearranging Equation~\ref{eq:main}, it follows that
  %
  \begin{align*}
    \Phi _n (q) \,\, | \,\, (q - 1)
  \end{align*}
  %
  Importantly, this means that when considering this polynomial over $\C$, that
  $|\Phi _n(q)| \leq | q - 1 | = q - 1$; the latter equality arising from the
  fact, that $q \in \N$. We now have everything that we need to coerce a
  contradiction if $n > 1$.

  Firstly, we write $\Phi _n (q)$ in its product form $\Phi _n (q) = \prod _i (q
  - \zeta_i)$ where $\zeta_i$ are the primitive $n$-th roots of unity. We
  then note
  %
  \begin{align}\label{eq:inequality}
    |\Phi _n (q)|
    = \left| \prod _{i=1} ^{\varphi(n)} (q - \zeta _i) \right|
    = \prod _{i=1} ^{\varphi(n)} |q - \zeta _i|
    \overset{(*)}{\geq} \prod _{i=1} ^{\varphi(n)} \left| |q| - |\zeta _i| \right|
    = \prod _{i=1} ^{\varphi(n)} |q - 1|
    = (q - 1) ^{\varphi(n)}
    \geq q - 1
  \end{align}
  %
  where $\varphi$ is Euler's totient Function
  (Definition~\ref{def:euler_totient}). Now we are posed a problem, we have on
  the one hand that $\Phi _n (q) | (q^n - 1) \implies |\Phi _n (q)| \leq q -1$,
  yet on the other hand $|\Phi _n (q)| \geq q - 1$ by the previous calculation.
  So we conclude $|\Phi _n (q)| = q - 1$ which happens if and only if both
  inequalities in~\ref{eq:inequality} are in fact equalities. We can demonstrate
  that $(*)$ is a strict inequality when $n > 1$: For an $n$-th root of unity
  $\zeta \neq 1$, it holds true that
  %
  \begin{align*}
    |q - 1| < |q - \zeta|
  \end{align*}
  %
  for $q \in \N$. This is clearest, when seen geometrically
  %
  \begin{center}
    \begin{tikzpicture}
      [
        scale=1,
        >=stealth,
        point/.style = {draw, circle,  fill = black, inner sep = 1pt},
        dot/.style   = {draw, circle,  fill = black, inner sep = 0.2pt},
      ]

      % The circle
      \def\rad{1.5}
      \node (origin) at (0,0) [point, label = {below right:$\mathcal{O}$}]{};
      \draw[dashed] (origin) circle (\rad);

      % The Axes
      \draw[->] (-1.25*\rad,0) -- (4.5*\rad,0);
      \draw[->] (0,-1.25*\rad) -- (0, 1.25*\rad);

      % Points
      \node (w) at +(60:\rad) [point, label = {above right:$\omega_n$}] {};
      \node (q) at (3*\rad,0) [point, label = {below:$q$}] {};
      \node (u) at (\rad,0)   [point, label = {below right:$1$}] {};

      \node at (1*\rad,0)   [point, label = {below right:$1$}] {};
      \node at (2*\rad,0)   [point, label = {below:$2$}] {};
      \node at (4*\rad,0)   [point, label = {below:$4$}] {};

      % Line connecting
      \draw []  (w) -- (q) -- (u) -- (w);
    \end{tikzpicture}
  \end{center}
  %
  So when $n > 1$, there is a primitive root of unity $\neq 1$, and thus, the
  inequality strict. This is cause of our contradiction, so $n=1$ which in turn
  dictates $F=A$, hence all elements in $A$ commute and $A$ is a field.
\end{proof}
