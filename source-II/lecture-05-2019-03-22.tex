\setcounter{chapter}{4}
\chapter{Fundamental Theorem of Galois Theory}


We discussed three equivalent characterisations of what it means to be a Galois
extension last lecture. Now we will introduce the \textbf{Fundamental Theorem of
Galois Theory}. This theorem is served in three parts. Naturally, we begin with

\section{Part 1 - Bijective Correspondence}

As always, let $E/F$ be a Galois extension, and $G = \Gal(E/F)$. We have seen
that when there is a subextension $E/K/F$, then $\Aut(E/K)$ is a subgroup of
$G$. We want to formalise this idea. In fact we will show that there is a
bijection between the set of all subextensions of a Galois extension, and the
subgroups of the Galois group.

\begin{remark}
  This idea gives rise to the alternative notation of an extension:
  \begin{center}
    \begin{tikzpicture}
      \matrix(m)[%
        matrix of math nodes,
        row sep=0.25em,
        column sep=4em,
        minimum width=1em
      ]{%
        E & \Gal(E/E) = \set{\id} \\
          & \rotatebox[origin=c]{-90}{$\subseteq$} \\
        K & \Gal(E/K) = S \\
          & \rotatebox[origin=c]{-90}{$\subseteq$} \\
        F & \Gal(E/F) = G \\
      };
      \path[-stealth]
        (m-1-1) edge [-] (m-3-1)
        (m-3-1) edge [-] (m-5-1)

        (m-1-1) edge [draw,-,decorate,decoration={snake}]
                node [below] {\tiny corresponds} (m-1-2)
        (m-3-1) edge [draw,-,decorate,decoration={snake}]
                node [below] {\tiny corresponds} (m-3-2)
        (m-5-1) edge [draw,-,decorate,decoration={snake}]
                node [below] {\tiny corresponds} (m-5-2)
        ;
    \end{tikzpicture}
  \end{center}
\end{remark}

\begin{theorem}
  Let $E/F$ be a Galois extension, and $G$ be the associated Galois group. Then
  the set of all subgroups of $G$ is isomorphic to the set of subextensions.
\end{theorem}

\begin{proof} (Construction of an isomorphism)
  We will approach this proof by showing that if we begin with a subextension,
  then find the corresponding automorphism group, and then look at the fixed
  field of that automorphism group, we expect to end up with the initial
  subfield. We will then show that if we begin with a subgroup, then find the
  corresponding fixed field and then look at the automorphism group of that
  fixed field, we expect to end up with the initial subgroup.

  Let $E/K/F$ be a subextension. We need to show that the fixed field of
  $\Aut(E/K)$, $H$ is $K$, as \textit{a priori} we only know that $K
  \subseteq H$. By the first equivalence, this means that $E/K$ must be a Galois
  extension. Let us prove this general

  \begin{siderules}
    \begin{theorem}[Subextensions of Galois extensions are Galois]
      Let $E/F$ be a Galois extension, with a subextension $E/K/F$ then $E/K$ is
      also a Galois Extension.
    \end{theorem}
    \begin{proof}
      We will make use of the third equivalence: As $E/F$ is Galois, it is also
      normal. Let $p \in F[Z]$ with distinct roots such that $E$ is the
      splitting field. Clearly $p \in K[Z] \supseteq F[Z]$ and thus $E/K$ is
      Galois.
    \end{proof}
  \end{siderules}

  This concludes one direction of our proof. Now we proceed in the other
  direction. To that end, let $S \leq G$ be a subgroup in $G$, and $K$ the field
  fixed by $S$. Consider the subextension $E/K/F$, and look at the automorphism
  group $\Aut(E/K)$. We want to show that $S = \Aut(E/K)$. This is easily done,
  as by definition, $K$ is Galois, and thus $\Aut(E/K) = \Gal(E/K) = S$.
\end{proof}

This is a handy application of Galois theory, and gives us a new way to view
subextensions, namely as subgroups of the automorphism group.

\section{Part 2 - Numerics}

\begin{theorem}
  If $E/K/F$ is a subextension of a Galois extension $E/F$, then $\dim(E/K) =
  |\Aut(E/K)|$.
\end{theorem}

\begin{proof}
  This is true as a direct result of the theorem proved above: we now know that
  a subextension of a Galois extension is also Galois, and by use of the second
  equivalence, we have the claim. However, we also have a new way to think about
  this extension, namely as a subgroup of the automorphism group. Here we know
  from group theory, that
  \begin{align*}
    |G:S|
    = \frac{|G|}{|S|}
    = \frac{|\Aut(E/F)|}{|\Aut(E/K)|}
    = \frac{\dim(E/F)}{\dim(E/K)}
    = \dim(K/F)
    = |K:F|
  \end{align*}
  where on the left, we are referring to the group index, and on the right the
  extension dimension.
\end{proof}

It is a satisfying result to see that these two notations turn out to be
compatible.

\section{Part 3 - Normal Extensions and Normal Subgroups}

We have seen that every subextension $E/K/F$ of a Galois extension is Galois,
but what about the extension $K/F$? We claim that

\begin{theorem}\label{thm:ftgt_normal}
  For a Galois extension $E/F$ with $G = \Gal(E/F)$ and a subextension $E/K/F$
  with $S = \Aut(E/K)$, $K/F$ is a Galois extension if and only if $S$ is normal
  in $G$.
\end{theorem}

\begin{proof}
  We begin with $K/F$ being Galois. We have for any element $g \in G$ that $F$
  is fixed,

  \begin{center}
    \begin{tikzpicture}
      \matrix(m)[%
        matrix of math nodes,
        row sep=2em,
        column sep=2em,
        minimum width=2em
      ]{%
        E &   & E \\
          & F &   \\
      };
      \path[-stealth]
        (m-1-1) edge [<->] node [above] {$\overset{g}{\cong}$} (m-1-3)
        (m-2-2) edge [-] (m-1-1)
        (m-2-2) edge [-] (m-1-3)
        ;
    \end{tikzpicture}
  \end{center}
  %
  and we can ask ourselves what happens to $K$ under $g$
  %
  \begin{center}
    \begin{tikzpicture}
      \matrix(m)[%
        matrix of math nodes,
        row sep=2em,
        column sep=2em,
        minimum width=2em
      ]{%
        E &   &   &   & E \\
          & K &   & ? &   \\
          &   & F &   &   \\
      };
      \path[-stealth]
        (m-1-1) edge [<->] node [above] {$\overset{g}{\cong}$} (m-1-5)
        (m-2-2) edge [->] node [above] {$g$} (m-2-4)

        (m-3-3) edge [-] (m-2-2)
        (m-2-2) edge [-] (m-1-1)
        ;
    \end{tikzpicture}
  \end{center}

  We claim that $g$ \textit{preserves} $K$. This means that $g(K) = K$ but
  \textbf{not} that $g|_K = \id$. If the latter were true, then $g \in
  \Aut(E/K)$ which clearly needn't be the case.

  \begin{siderules}
    \begin{theorem}[The Galois group preserves subfields which are also Galois
      over the base field]
      Let $E/F$ be a Galois extension, $E/K/F$ a subextension with $K/F$ also
      Galois. Then for $g \in \Gal(E/F)$ it holds that $g(K) = K$.
    \end{theorem}
    \begin{proof}[Proof $g$ preserves $K$]
      By the third equivalence, $K/F$ is also normal, and hence $K$ is the
      splitting field of some polynomial $p \in F[Z]$ with distinct roots
      $\{r_1, \ldots, r_k\}$ in $K$. These roots generate $K$, so it suffices to
      look at the image of these roots. As the coefficients of $p$ lie in $F$,
      $g$ fixes them and
      %
      \begin{align*}
        0
        = g(0)
        \overset{\mathrm{root}}{=} g(p(r_1))
        \overset{\mathrm{hom}}{=} p(g(r_1)).
      \end{align*}
      %
      We see that $g(r_1)$ is again a root, meaning that $g$ is simply acts as a
      permutation on the roots. A permutation of a generating set doesn't change
      the span, and thus we may conclude that $g(K) = K$.
    \end{proof}
  \end{siderules}

  We can now proceed to prove that $S$ is normal in $G$. To this, we must show
  that $\forall s \in S, g \in G: g^{-1} s g \in S$. As we now know that $g(K) =
  K$, we have
  \begin{center}
    \begin{tikzpicture}
      \matrix(m)[%
        matrix of math nodes,
        row sep=2em,
        column sep=4em,
        minimum width=2em
      ]{%
        K & K & K & K \\
      };
      \path[-stealth]
        (m-1-1) edge [->] node [above] {$g$} (m-1-2)
        (m-1-2) edge [->]
                node [above] {$s|_K = \id$} (m-1-3)
        (m-1-3) edge [->] node [above] {$g^{-1}$} (m-1-4)
        ;
    \end{tikzpicture}
  \end{center}
  as $g$ preserves $K$ and $s \in \Aut(E/K)$.

  In other words,
  \begin{align*}
             & (g s g^{-1})|_K  = g|_K \circ s |_K \circ g^{-1}|_K = \id \\
    \implies & g s g^{-1} \in S                                          \\
    \implies & S \triangleleft G
  \end{align*}
  and we have the first direction of our proof.

  Let us now proceed with showing the other direction. To that end, let $S
  \triangleleft G$ be normal. To show that the field fixed by $S$, $K$ is Galois
  over $F$, we will make a similar argument as before, and first show that $g(K)
  = K$.
  \begin{siderules}
    \begin{theorem}[The fixed field of a normal subgroup of the Galois group is
      preserved by the Galois group]
      Let $E/F$ be a Galois extension, and let $S \triangleleft G = \Gal(E/F)$,
      then the field fixed by $S$, call this $K$, is \textit{preserved} by the
      Galois group.
    \end{theorem}
    \begin{proof}[Proof $g$ preserves $K$]
      Lets assume, indirectly, that $g$ does not preserve $K$.  Then there
      exists some $x \in K$, such that $y := g(x) \notin K$.  Now that we're no
      longer in $K$, $S$ need not fix $y$ and there is some $s \in S$, that
      moves $y$ to some $z \neq y$. With this we have $sg(x) = z \notin K$.
      However, owing to the normality of $S$, we know that for $sg$ there exists
      a $t \in S$ such that $gt = sg \iff g^{-1} s g \in S$. We now have a
      contradiction on our hands, as
      \begin{align*}
        y = g(x) \overset{t |_K = \id}{=} g t (x) = s g (x) = s(y) = z
      \end{align*}
      but $y \neq z$.
      \begin{figure}[H]
        \centering
          % \caption{Picture to Proof}
          \incfig{source-II/figures/fixed-field-preserved}
      \end{figure}
    \end{proof}
  \end{siderules}

  We now want to use the second equivalence to show that $K/F$ is Galois. We can
  do this by looking at the homomorphism of restriction onto $K$
  \begin{align*}
    \Phi{:}\, G \to S = \Aut(K/F); g \mapsto g|_K
  \end{align*}
  and by noting that $S = \ker(\Phi)$, we see that
  \begin{align*}
    G/S \hookrightarrow \Aut(K/F); gS \mapsto \Phi(g)
  \end{align*}
  injects and thus
  \begin{align*}
    |\Aut(K/F)| \geq |G/S| = \frac{|G|}{|S|}.
  \end{align*}
  On the other hand, by \textbf{part 2}, we know that $\dim(K/F) =
  \frac{|G|}{|S|}$ and with Lecture 3, we have
  \begin{align*}
    \frac{|G|}{|S|} = \dim(K/F) \geq |\Aut(K/F)| \geq \frac{|G|}{|S|}.
  \end{align*}
  We may conclude that $\dim(K/F) = |\Aut(K/F)|$ and thus $K/F$ is Galois.
\end{proof}

\begin{remark}
  To know that $\Phi$ is well defined (i.e. $g|_K \in \Aut(F/K$)), we use that
  $g$ preserves $K$, and $g$ is an isomorphism over $E \supseteq K$.
\end{remark}

\begin{corollary}
  For $G$ the Galois group of some field extension $E/F$, every subfield $K$
  that creates a subfield extension $E/K/F$ is preserved by $G$.
\end{corollary}

\begin{remark}
  This again shows the compatibility between terms that we learned in group
  theory and field extensions. A subgroup of the automorphism group is normal,
  and then the corresponding fixed field is also normal (i.e. Galois).
\end{remark}
