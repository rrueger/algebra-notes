\setcounter{chapter}{1}
\chapter{Dimension bound}

\section{Characters}

\begin{definition}[Character]
  Let $G$ be a group and $F$ a field. Define a \textbf{character} to be a group
  homomorphism $\sigma \colon G \to F^\times$. We say ``\textit{$\sigma$ is a
  character of $G$ on $F$}''.
\end{definition}

\begin{example}[Character]
  \begin{align*}
    \sigma \colon \left( \rquot{\Z}{2\Z}, +, 0 \right) \to \C^\times
    \quad
    \text{with}
    \quad
    \sigma(\overline{0}) = 1
    \quad
    \sigma(\overline{1}) = -1
  \end{align*}
\end{example}

\begin{definition}[Linearly dependent, Linearly independent]
  Mutually distinct characters $\sigma_1, \ldots, \sigma_n \colon G \to
  F^\times$ are \textbf{linearly dependent} if there are $a_1, \ldots, a_n \in
  F$ not all zero, such that
  %
  \begin{align*}
    a_1 \sigma_1 + \cdots + a_n \sigma_n \equiv 0
  \end{align*}
  %
  If a set of characters are not linearly dependent, they are \textbf{linearly
  independent}.
\end{definition}

\begin{remark}
  We way $\sigma \equiv 0$, when $\sigma$ represents the zero-character:
  $\forall x \in F: \sigma(x) = 0$
\end{remark}

\begin{example}
  For any non-zero character $\set{\sigma}$ is linearly independent and
  $\set{\sigma, \sigma}$ is linearly dependent.
\end{example}

\begin{theorem}[Independence of distinct characters]
  % Let $\sigma_1, \ldots, \sigma_n : G \to F^\times$ be a set of characters. If
  % they are mutually distinct, they are linearly independent.
  A set of mutually distinct characters is linearly independent.
\end{theorem}

\begin{remark}
  This result is somewhat surprising, as in linear algebra, where we have seen
  the concept of linear dependence before, this statement is not true.
\end{remark}

\begin{proof}
  We will prove this over induction on the number of characters. To that end,
  let $\sigma_1, \ldots, \sigma_n : G \to F^\times$ be a set of mutually
  distinct characters. For $n=1$ the statement is trivially true. Now assume it
  is true for all $k < n$, and consider the case of $n$ mutually distinct
  characters. Suppose by contradiction, that $\sigma_1, \ldots, \sigma_n$ are
  linearly dependent. This means there exist some $a_1, \ldots, a_n$ not all
  zero such that
  %
  \begin{align*}
    a_1 \sigma_1 + \cdots + a_n \sigma_n \equiv 0
  \end{align*}
  %
  In fact, we know that no $a_i$ is zero, as then we would have linear
  dependence on $n-1$ characters, which is ruled out by the induction
  hypothesis. We can thus multiply by the inverse $a_n^{-1}$ to obtain
  %
  \begin{align*}
    \left( \sum _{i=1} ^{n-1} (a_n^{-1}a_i) \sigma_i \right) + \sigma_n \equiv 0
  \end{align*}
  %
  defining $b_i := a_n^{-1}a_i$, we get
  %
  \begin{align*}
    \left( \sum _{i=1} ^{n-1} b_i \sigma_i \right) + \sigma_n \equiv 0
  \end{align*}
  %
  In particular, for any $g \in G$, we have
  %
  \begin{align}
    \left( \sum _{i=1} ^{n-1} b_i \sigma_i(g) \right) + \sigma_n(g) = 0
  \end{align}
  %
  We will now use the fact that they are mutually distinct to find some $h \in
  G$ such that $\sigma_1(h) \neq \sigma_n(h)$, and get for $g \in G$
  %
  \begin{align}
    \left( \sum _{i=1} ^{n-1} b_i \sigma_i(hg) \right) + \sigma_n(hg) = 0 \\
    %
    \left( \sum _{i=1} ^{n-1} b_i \sigma_i(h) \sigma_i(g) \right) +
    \sigma_n(h)\sigma_n(g) = 0 \\
    %
    \left( \sum _{i=1} ^{n-1} b_i {\sigma_n(h)}^{-1} \sigma_i(h) \sigma_i(g)
    \right) + \sigma_n(g) = 0
  \end{align}
  %
  We now have two equations (2.1) and (2.4) which we can subtract, to obtain
  %
  \begin{align*}
    \sum _{i=1} ^{n-1} \sigma_i(g) b_i \left( \sigma_i(h){\sigma_n(h)}^{-1} -
    1 \right) = 0
  \end{align*}
  %
  all coefficients $b_i \left( \sigma_i(h){\sigma_n(h)}^{-1} - 1 \right)$ must
  be zero, otherwise we would have linear dependence on $n-1$ characters. In
  particular, for $i=1$, $b_1 = a_1{a_n}^{-1} \neq 0$
  %
  \begin{align*}
    b_1 \left( \sigma_1(h) {\sigma_n(h)}^{-1} -1 \right) = 0 \\
    \sigma_1(h) {\sigma_n(h)}^{-1} -1 = 0 \\
    \sigma_1(h) = \sigma_n(h)
  \end{align*}
  %
  Which is a contradiction.
\end{proof}

Let $E$ be a field, and $\sigma \colon E \to E$ an automorphism of the field. We
see that $\sigma \colon E^\times \to E^\times$ is a character of $G = (E^\times,
\cdot, 1)$. In particular, if $\sigma_1, \ldots, \sigma_n$ are mutually distinct
automorphisms, $\sigma_1, \ldots, \sigma_n \colon E^\times \to E^\times$ are
mutually distinct characters. As a corollary, $\sigma_1, \ldots, \sigma_n \colon
E^\times \to E^\times$ are linearly independent.

\section{Automorphisms and Fixed Fields}

\begin{definition}[Fixed field]
  Let $E$ be a field, and $\Sigma = \set{\sigma_1, \ldots, \sigma_n}$ a set of
  automorphisms on $E$. We define $H \subseteq E$ the \textbf{fixed field} of
  $\Sigma$ by
  %
  \begin{align*}
    H = \set{e \in E:\forall i \in {1, \ldots, n}: \sigma_i(e) = e}
  \end{align*}
\end{definition}

\begin{remark}
  Some authors denote the fixed field of a set of automorphisms $G$ on a field
  $E$ by $E^G$.
\end{remark}

\begin{theorem}\label{thm:fixed_field_is_field}
  $H$ is a subfield of $E$.
\end{theorem}

\begin{exercise}
  Prove Theorem~\ref{thm:fixed_field_is_field}
\end{exercise}

\section{Dimension of a Field Extension}

\begin{theorem}[Dimension Bound]
  Let $E$ be a field and $\Sigma \subseteq \Aut(E)$ a set of distinct
  automorphisms of $E$. Now let $H \subseteq E$ be the fixed field of $\Sigma$.
  Then
  %
  \begin{align*}
    \dim(E/H) \geq |\Sigma|
  \end{align*}
\end{theorem}

\begin{proof}\label{thm:dimension_bound}
  Let $\Sigma = \set{\sigma_1, \ldots, \sigma_n}$. Assume indirectly, that $r :=
  \dim(\rquot{E}{H}) < n$.  Then pick a basis $w_1, \ldots, w_n$ of $E/H$. Now
  let's write a matrix $M \in \Mat _{r \times n}(E)$
  %
  \begin{align*}
    M_{ij} = \sigma_j (w_r)
  \end{align*}
  %
  As $r < n$, $M:E^n \to E^r$ has  a non-trivial kernel. Let $a = {(a_1, \ldots,
  a_n)}^{\text{T}} \in \ker (M) \subseteq E^n$ be non-zero. Clearly we then have
  $Ma = 0$. Let us now take a step back, and remember that $w_1, \ldots, w_n$ is
  a basis of $E$, so for any $v \in E$, there exist some $\lambda = {(\lambda_1,
  \ldots, \lambda_n)}^\text{T} \in H^n$ such that $v = \sum _{j=1} ^n \lambda_i
  w_i$.  What happens when we consider $\lambda^\text{T}Ma = 0$? Considering
  only the first entry, we get
  %
  \begin{align*}
    0
    = \lambda_1 \sum _{i=1} ^r a_i \sigma_i (w_i)
    = \sum _{i=1} ^r a_i \sigma_i(\lambda_1) \sigma_i(w_i)
    = \sum _{i=1} ^r a_i \sigma_i(\lambda_1 w_i)
  \end{align*}
  %
  \begin{remark}
    Here we used that $\lambda_1 \in H$, thus $\sigma_i$ fixes it.
  \end{remark}
  %
  \noindent
  Continuing, we can now sum over all the entries
  %
  \begin{align*}
    0
    = \sum _{j=1} ^n \sum _{i=1} ^r a_i \sigma_i(\lambda_j w_i)
    = \sum _{i=1} ^r \sum _{j=1} ^n a_i \sigma_i(\lambda_j w_i)
    = \sum _{i=1} ^r a_i \sigma_i \left( \sum _{j=1} ^n\lambda_j w_i \right)
    = \sum _{i=1} ^r a_i \sigma_i (v)
  \end{align*}
  %
  In fact, as $a$ was chosen independently of $v$, this holds true for all $v
  \in E$, thus
  %
  \begin{align*}
    \sum _{i=1} ^r a_i \sigma_i \equiv 0
  \end{align*}
  %
  and we have a contradiction on our hands, as we have linear dependence of
  distinct characters.
\end{proof}

\begin{corollary}[Automorphism bound]
  Consider a field extension $E/F$. Let $H \subseteq E$ be the fixed field by
  $\Aut(E/F)$. Then
  %
  \begin{align*}
    \dim(E/F) \geq |\Aut(E/F)|
  \end{align*}
\end{corollary}

\begin{proof}
  % @todo: Explain more about this tower
  We see that $E/H/F$ is a tower of extensions, it follows that
  %
  \begin{align*}
    \dim(E/F) \geq \dim(E/H) \geq |\Aut(E/F)|
  \end{align*}
  %
  The second inequality coming about, by applying
  Theorem~\ref{thm:dimension_bound}.
\end{proof}
