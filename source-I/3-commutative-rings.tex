\chapter{Commutative Rings}

\section{Definition of Rings and Domains}

\begin{definition}[Ring]
  A \textbf{ring} $R$ is a set with two binary operations $R \times R \to R$:
  addition $(a,b) \mapsto a+b$ and multiplication $(a,b) \mapsto ab$, such that

  \begin{enumerate}

    \item $R$ is an abelian group under addition; that is,

      \begin{enumerate}
        \item $a + (b + c) = (a + b) + c$ for all $a,b,c \in R$;
        \item there is an element $0 \in R$ with $0 + a = a$ for all $a \in R;$
        \item for each $a \in R$, there is $a' \in R$ with $a' + a = 0$;
        \item $a + b = b + a$.
      \end{enumerate}

    \item \textbf{Associativity:} $a(bc) = (ab)c$ for every $a,b,c \in R$;
	
	\item there is $1 \in R$ with $1 a=a=a 1$ for every $a \in R$;

    \item\textbf{Distributivity:} $a(b+c) = ab+ ac$ and $(b+c)a = ba + ca$ for
      every $a,b,c \in R$.

  \end{enumerate}
\end{definition}

\begin{proposition}[Elementary properties]
  Let $R$ be a ring.

  \begin{enumerate}

    \item $0 \cdot a = 0 = a \cdot 0$ for every $a \in R$.

    \item If $1 = 0$ then $R$ consists of the single element $0$ (\textbf{zero
      ring}, or \textbf{trivial ring}).

    \item If $-a$ is the additive inverse of $a$, then $(-1)(-a) = a =
      (-a)(-1)$, in particular $(-1)(-1) = 1$.

    \item $(-1)a = -a = a(-1)$ for all $a \in R$.

    \item If $n \in \N$ and $n \cdot 1 = 0$, then $n \cdot a = 0$ for all $a
      \in R$, where
      \begin{align*}
        n \cdot a = \underbrace{a + a \ldots a}_{n \text{~summands}}
      \end{align*}

  \end{enumerate}

\end{proposition}

\begin{definition}[Subring, Proper subring]
  A subset $S$ of a ring $R$ is a \textbf{subring} of $R$ if
  %
  \begin{enumerate}
    \item $1 \in S$,
    \item if $a,b \in S$, then $a-b \in S$,
    \item if $a,b \in S$, then $ab \in S$.
  \end{enumerate}
  %
  A \textbf{proper subring} $S \subseteq R$ is a subring of $R$ with $S \neq
  R$.
\end{definition}


\begin{definition}[Domain, Integral domain]
  A \textbf{domain} (or integral domain) is a nonzero commutative ring $R$ that
  satisfies the \textbf{cancellation law}: for all $a,b,c \in  R$, if $ca = cb$
  and $c \neq 0$, it follows $a = b$.
\end{definition}

\begin{proposition}
  A nonzero commutative ring $R$ is a domain if and only if the product of two
  nonzero elements of $R$ is nonzero.
\end{proposition}

\begin{proposition}
  The commutative ring $\Z_m$ is a domain if and only if $m$ is prime.
\end{proposition}

\noindent
\textbf{\textsc{From here on, all rings are assumed to be commutative.}}

\begin{definition}[Divisior]
  Let $a$ and $b$ be elements of a commutative ring $R$. Then $a$
  \textbf{divides} $b$ in $\mathbf{R}$, denoted by
  %
  \begin{align*}
    a \mid b,
  \end{align*}
  %
  if there exists an element $c \in R$ with $b = ca$.
\end{definition}

\begin{definition}[Unit]
  An element $u$ in a commutative ring $R$ is called a \textbf{unit} if $u \mid
  1$ in $R$, that is, if there exists $v \in R$ with $uv = 1$.
\end{definition}

\begin{proposition}
  If $a$ is an integer, then $[a]$ is a unit in $\Z_m$ if and only if $a$ and
  $m$ are relatively prime. In fact, if $sa  + tm = 1$, then $[a]^{-1} = [s]$.
\end{proposition}

\begin{corollary}
  If $p$ is prime, then every nonzero $[a] \in \Z_{p}$ is a unit.
\end{corollary}

\begin{definition}[The group of units]
  If $R$ is a nonzero commutative ring, then the \textbf{group of units} of $R$
  is
  %
  \begin{align*}
    U(R)= \set{\text{all units in~}R}
  \end{align*}
  %
\end{definition}

\begin{definition}[Field]
  A \textbf{field} $F$ is a nontrivial commutative ring in which every nonzero
  element $a$ is a unit; that is, there is  $a^{-1} \in F$ with $a^{-1} a = 1$.
\end{definition}

\begin{proposition}
  The commutative ring $\Z_m$ is a field if and only if $m$ is prime.
\end{proposition}

\begin{proposition}
  Every field $F$ is a domain.
\end{proposition}

\begin{proposition}
  Every finite domain is a field.
\end{proposition}

\begin{theorem}[Existence of the fraction field]
  \label{thm:existenceoffractionfield}
  If $R$ is a domain, then there is a field containing $R$ as a subring.
  Moreover, such a field $F$ can be chosen so that, for each $f \in F$ there are
  $a,b \in R$ with $b \neq 0$ and $f = ab^{-1}$.
\end{theorem}

\begin{idea}
  To prove this theorem, we constructed the following relation on $R \times
  R^{\times}$
  %
  \begin{align*}
  \forall a,c \in R, b,d \in R^{\times}:\, (a,b) \equiv (c,d)
    \overset{\text{Def.}}{\iff} ad = bc
  \end{align*}
  %
  It is easy to see, that this relation is indeed an equivalence relation. We
  define $F$ as the set of all equivalence classes $[a,b]$ and we equip $F$ with
  the following addition and multiplication
  %
  \begin{align*}
    [a,b] + [c,d] = [ad + bc, bd] \text{~and~} [a,b][c,d] = [ac, bd]
  \end{align*}
  %
  It is easy to verify, that these operations are well-defined and that $F$ is a
  field. Now $R' = \{[a,1]:a \in R\}$ is a subring and we identify $a \in R$
  with $[a,1] \in R'$.
\end{idea}

\begin{definition}[Fraction field]
  The field $F$ constructed from $R$ in
  Theorem~\ref{thm:existenceoffractionfield} is called the \textbf{fraction
  field} of $R$, denoted by $\mathrm{Frac}(R)$.
\end{definition}

\begin{theorem}[Universal property of the fraction field]
  For $R$ an integral domain, $K$ a field and $\varphi \colon R \to K$ an
  injective ring homomorphism, there exists a unique $\overline{\varphi} \colon
  F = \Frac (R) \to K$ such that the following diagram commutes
  %
  \begin{figure}[H]
    \centering
    \begin{tikzpicture}
      \matrix (m)[matrix of math nodes,row sep=3em,column sep=2em]{%
        R &   & F \\
          & K & \\
      };

      \path[-stealth]
        (m-1-1) edge node [above] {$\imath$} (m-1-3)
        (m-1-1) edge node [below left] {$\varphi$} (m-2-2)
        (m-1-3) edge node [below right] {$\overline{\varphi}$} (m-2-2);
    \end{tikzpicture}
  \end{figure}
  %
  \noindent
  i.e\ that $\varphi = \imath \circ \overline{\varphi}$. Here $\imath \colon R
  \to \Frac (R)$ is the natural inclusion $r \mapsto \frac{r}{1}$. The unique
  map is is given by
  %
  \begin{align*}
    \overline{\varphi} \left( \frac{r}{s} \right)
    := \varphi (r) {\varphi (s)}^{-1}.
  \end{align*}
\end{theorem}

\begin{definition}[Subfield]
  A \textbf{subfield} of a field $K$ is a subring $k$ of $K$ that is also a
  field.
\end{definition}


\section{Polynomials}

\begin{definition}[Formal power series]
  If $R$ is a commutative ring, then a \textbf{formal power series} over $R$ is
  a sequence of elements $s_i \in R$ for all $i \geq 0$, called the
  \textbf{coefficients} of $\sigma$:
  %
  \begin{align*}
    \sigma = (s_0, s_1, s_2, \ldots, s_i, \ldots).
  \end{align*}
  %
\end{definition}

\begin{definition}[Polynomial, Leading Coefficient, Degree]
  A \textbf{polynomial} over a commutative ring $R$ is a formal power series
  $\sigma=(s_0,s_1, \ldots, s_i, \ldots)$ over $R$ for which there exists some
  integer $n \geq 0$ with $s_i = 0$ for all $i > n$; that is,
  %
  \begin{align*}
    \sigma =(s_0, s_1, \ldots, s_n, 0,0,\ldots).
  \end{align*}
  %
  if $s_n \neq 0 $ then $s_n$ is called the \textbf{leading coefficient} and $n$ is the
  \textbf{degree}: $n = \deg(\sigma)$. If $s_n = 1$, we call $\sigma$ monic.
\end{definition}

\begin{remark}[Definition of degree of the zero polynomial]
  By this definition, the degree of the zero polynomial $(0, \ldots)$ is not defined.
  Some authors choose to define it as being $-\infty$. This makes sense later in
  the context of the degree formulae
  (Lemma~\ref{commutative-rings:degree-formulae}), as we will not have to
  require the polynomials to be nonzero for the product or sum formulae.
  Ultimately this is down to convention.
\end{remark}

\begin{remark}[Notation]
  If $R$ is a commutative ring, then $R[[x]]$ denotes the set of all formal
  power series over $R$, and $R[x] \subseteq R[[x]]$ denotes the set of all
  polynomials over $R$.
\end{remark}

\begin{proposition}
  If $R$ is a commutative ring, then $R[[x]]$ is a commutative ring that
  contains $R[x]$ and $R'$ as subrings where $R' = \set{(r,0,0,\ldots): r \in R}
  \subset R[x]$.
\end{proposition}

\begin{lemma}\label{commutative-rings:degree-formulae}
  Let $R$ be a commutative ring and let $p,q \in R[x]$ be nonzero polynomials.
  %
  \begin{enumerate}
    \item Either $pq = 0$ or $\deg(pq) \leq \deg(p) + deg(q)$.
    \item If $R$ is a domain, then $pq \neq 0$ and
      \begin{align*}
        \deg(pq) = deg(p) + deg(q).
      \end{align*}
    \item If $R$ is a domain, $p,q \neq 0$ and $q \mid p$ in $R[x]$, then
      $\deg(q) \leq \deg(p)$.
    \item If $R$ is a domain, then $R[x]$ is a domain.
  \end{enumerate}
\end{lemma}

\begin{definition}[Interdeterminate]
  The \textbf{indeterminate} $x \in R[x]$ is
  %
  \begin{align*}
    x = (0,1,0,0, \ldots).
  \end{align*}
\end{definition}

\begin{lemma}
  The indeterminate $x$ in $R[x]$ has the following properties.
  %
  \begin{enumerate}
    \item If $p = (s_0,s_1,\ldots)$, then
      \begin{align*}
        x p = (0, s_0, s_1, \ldots);
      \end{align*}
      that is, multiplying by $x$ shifts each coefficient one step to the right.
  \item If $n \geq 0$, then $x^n$ is the polynomial having $0$ everywhere except
    for $1$ in the nth coordinate.
  \item If $r \in R$, then
    \begin{align*}
      (r,0,0,\ldots)(s_0, s_1, \ldots, s_j, \ldots)
      = (r s_0, r s_1, \ldots, r s_j, \ldots).
    \end{align*}
  \end{enumerate}
\end{lemma}

\begin{corollary}
  Formal power series $s_0 + s_1 x + s_2 x^2 + \ldots$ and $t_0 + t_1 x + t_2
  x^2 + \ldots$ in $R[[x]]$ are equal if and only if $s_i = t_i$ for all $i$.
\end{corollary}

\begin{definition}[Field of rational functions]
  Let $k$ be a field. The fraction field $\Frac(k[x])$ of $k[x]$, denoted by
  $k(x)$, is called the \textbf{field of rational functions} over $k$.
\end{definition}

\begin{proposition}
  If $k$ is a field, then the elements of $k(x)$ have the form $f(x) / g(x)$,
  where $f(x), g(x) \in k[x]$ and $g(x) \neq 0$.
\end{proposition}

\begin{proposition}
  If $p$ is prime, then the field of rational functions $\mathbb{F}_{p}(x)$ is
  an infinite field containing $\mathbb{F}_p$ as a subfield.
\end{proposition}


\section{Homomorphisms}

\begin{definition}[Ring homomorphism, Isomorphism]
  If $A$ and $R$ are (not necessarily commutative) rings, a \textbf{ring
  homomorphism} is a function $\varphi: A \to R$ such that
  \begin{enumerate}
    \item $\varphi(1) = 1$
    \item $\varphi(a + a') = \varphi(a) + \varphi(a')$ for all $a,a' \in A$,
    \item $\varphi(aa') = \varphi(a) \varphi(a')$ for all $a,a' \in A$.
  \end{enumerate}
  A ring homomorphism that is also a bijection is called an
  \textbf{isomorphism}.
\end{definition}

\begin{theorem}[Universal property of Rings]
  Let $R$ and $S$ be commutative rings, and let $\varphi \colon R \to S$ be a
  homomorphism. For fixed $s_1, \ldots, s_n \in S$, there exists a unique
  homomorphism
  %
  \begin{align*}
    \Phi \colon R[x_1, \ldots, x_n] & \to S \\
    x_i & \mapsto s_i
  \end{align*}
  %
  with $\Phi(r) = \varphi(r)$ for all $r \in R$. That is, $\Phi$ uniquely
  extends $\varphi$ from $R$ to $R[x_1, \ldots, x_n]$, sending $x_i$ to $s_i$.
\end{theorem}

\begin{definition}[Evaluation map]
  If $R$ is a commutative ring and $a \in R$, then \textbf{evaluation at} $a$ is
  the function $e_a: R[x] \to R$, defined by $e_a(f) = f(a)$.
\end{definition}

\begin{corollary}
  If $R$ is a commutative ring, then evaluation $e_a: R[x] \to R$ is a
  homomorphism for every $a \in R$.
\end{corollary}

\begin{corollary}
  If $R$ and $S$ are commutative rings and $\varphi: R \to S$ is a homomorphism,
  then there is a homomorphism $\varphi_{*}: R[x] \to S[x]$ given by
  %
  \begin{align*}
    \varphi_{*}: r_0 + r_1 x + r_2x^2 + \ldots
    \mapsto \varphi(r_0) + \varphi(r_1) x + \varphi(r_2)x^2 + \ldots
  \end{align*}
  %
  Moreover, $\varphi_{*}$ is an isomorphism if $\varphi$ is.
\end{corollary}

\begin{proposition}
  Let $\varphi: A \to R$ be a homomorphism.
  \begin{enumerate}
    \item $\varphi(a^n) = \varphi(a)^n$ for all $n \geq 0$ and all $a \in A$.
    \item If $a \in A$ is a unit, then $\varphi(a)$ is a unit and
      $\varphi(a^{-1}) = \varphi(a)^{-1}$ and so $\varphi(U(A)) \subseteq U(R)$,
      where $U(A)$ is the group of units of $A$. Moreover, if $\varphi$ is an
      isomorphism, then $U(A) \cong U(R)$ (as groups).
  \end{enumerate}
\end{proposition}

\begin{definition}[Kernel, Image]
  If $\varphi: A \to R$ is a homomorphism, then its \textbf{kernel} is
  %
  \begin{align*}
    \ker \varphi = \set{a \in A \text{~with~} \varphi(a) = 0}
  \end{align*}
  %
  and its \textbf{image} is
  %
  \begin{align*}
    \im \varphi = \set{r \in R: r = \varphi(a) \text{~for some~} a \in R}.
  \end{align*}
\end{definition}

\begin{definition}[Ideal, Proper ideal]
  An \textbf{ideal} in a commutative ring $R$ is a subset $I$ of $R$ such that
  %
  \begin{enumerate}
    \item $(I, +, 0) \subseteq R$ is a group
    \item $I$ is closed under multiplication from elements in $R$, i.e\ for $r
      \in R, a \in I: ra \in I$
  \end{enumerate}
  %
  The ring $R$ itself and $(0)$, the subset containing only $0$, are always
  ideals. An ideal $I \neq R$ is called a \textbf{proper ideal}.
\end{definition}

\begin{remark} \mbox{}\\
  \begin{enumerate}
    \item If $I \subseteq R$ contains $1$, then $I = R$.
    \item If $R$ is a field, the only ideals in $R$ are $(0)$ and $R$ itself,
      i.e.\ the only proper ideal in $R$ is $(0)$.
  \end{enumerate}
\end{remark}

\begin{proposition}
  A homomorphism $\varphi: A \to R$ is an injection if and only if $\ker \varphi
  = (0)$.
\end{proposition}

\begin{corollary}
  If $k$ is a field and $\varphi: k \to R$ is a homomorphism, where $R$ is not
  the zero ring, then $\varphi$ is an injection.
\end{corollary}

\begin{definition}[Principal ideal]
  If $b_1, b_2, \ldots , b_n$ lie in $R$, then the set of all linear
  combinations
  %
  \begin{align*}
      I = \set{r_1 b_1 + \ldots r_n b_n: r_i \in R}
  \end{align*}
  %
  is an ideal in $R$. We write $(b_1, b_2, \ldots, b_n)$. The ideal $(b)$ is
  called \textbf{principal ideal} generated by $b$.
\end{definition}

\begin{theorem}
  Every ideal $I$ in $\Z$ is a principal ideal; that is, there is $d \in \Z$
  with $I = (d)$.
\end{theorem}

\begin{proposition}
  Let $R$ be a commutative ring and let $a,b \in R$. If $a \mid b$ and $b \mid
  a$, then $(a) = (b)$.
\end{proposition}

\begin{definition}[Associates]
  Elements $a$ and $b$ in a commutative ring $R$ are \textbf{associates} if
  there exists a unit $u \in R$ with $b = ua$.
\end{definition}

\begin{proposition}
  Let $R$ be a domain and let $a,b \in R$.
  %
  \begin{enumerate}
    \item $a \mid b$ and $b \mid a$ if and only if $a$ and $b$ are associates.
    \item The principal ideals $(a)$ and $(b)$ are equal if and only if $a$ and
      $b$ are associates.
  \end{enumerate}
\end{proposition}


\section{Quotient Rings}

\noindent
Quotient rings are a generalization of the commutative rings $\Z_m$.

\begin{definition}[Coset]
  Let $I$ be an ideal in a commutative ring $R$. If $a \in R$, then the
  \textbf{coset} $a + I$ is the subset
  %
  \begin{align*}
    a + I =  \{a + i: i \in I\}.
  \end{align*}
  %
  The coset $a + I$ is often called $a \mod I$.
  %
  \begin{align*}
    R/I = \{a + I: a \in R\}.
  \end{align*}
\end{definition}

\begin{remark}
  Given an ideal $I$ in a commutative ring $R$, the relation $\equiv$ on $R$,
  defined by
  %
  \begin{align*}
    a \equiv b \text{~if~} a - b \in I
  \end{align*}
  %
  is called \textbf{congruence mod I}; it is an equivalence relation on $R$ and
  its equivalence classes are the cosets. The family of all cosets is a
  \textbf{partition} of $R$.
\end{remark}

\begin{proposition}
  Let $I$ be an ideal in a commutative ring $R$. If $a,b \in R$, then $a + I =
  b+ I$ if and only if $a - b \in I$. In particular, $a + I = I$ if and only if
  $a \in I$.
\end{proposition}

\begin{definition}[Addition and multiplication on quotients]
  Let $R$ be a commutative ring and $I$ be an ideal in $R$. Define addition
  $\alpha: R/I \times R/I \to R/I$ by
  %
  \begin{align*}
    \alpha:(a + I, b + I) \mapsto a+b+I,
  \end{align*}
  %
  and multiplication $\mu: R/I \times R/I \to R/I$ by
  %
  \begin{align*}
    \mu: (a+ I, b+I) \mapsto ab + I
  \end{align*}
\end{definition}

\begin{theorem}
  If $I$ is an ideal in a commutative ring $R$, then $R/I$ is a commutative ring
  and it is called \textbf{quotient ring} of $R$ modulo $I$.
\end{theorem}

\begin{definition}[Natural map]
  Let $I$ be an ideal in a commutative ring $R$. The \textbf{natural map} is the
  function $\pi: R \to R/I$ given by $a \mapsto a + I$.
\end{definition}

\begin{proposition}
  If $I$ is an ideal in a commutative ring $R$, then the natural map $\pi: R \to
  R/I$ is a surjective homomorphism and $\ker \pi = I$.
\end{proposition}

\begin{corollary}
  Given an ideal $I$ in a commutative ring $R$, there exists a commutative ring
  $A$ and a (surjective) homomorphism $\varphi: R \to A$ with $I = \ker
  \varphi$.
\end{corollary}

\begin{theorem}[First Isomorphism Theorem]
  Let $R$ and $A$ be commutative rings. If $\varphi: R \to A$ is a homomorphism,
  then $\ker \varphi$ is an ideal in $R$, $\im \varphi$ is a subring of $A$ and
  %
  \begin{align*}
    R/\ker \varphi \cong \im \varphi
  \end{align*}
\end{theorem}

\begin{definition}[Prime field]
  If $k$ is a field, the intersection of all the subfields of $k$ is called the
  \textbf{prime field} of $k$.
\end{definition}

\begin{proposition}
  Let $k$ be a field with unit $l$ and let $\chi: \Z \to k$ be the homomorphism
  $\chi: n \mapsto nl$.
  \begin{enumerate}
    \item Either $\im \chi \cong \Z$ or $\im \chi \cong \mathbb{F}_p$ for some
      prime $p$.
    \item The prime field of $k$ is isomorphic to $\mathbb{Q}$ or to
      $\mathbb{F}_p$ for some prime $p$.
  \end{enumerate}
\end{proposition}

\begin{definition}[Characteristic]
  A field $k$ has \textbf{characteristic} 0 if its prime field is isomorphic to
  $\mathbb{Q}$; it has characteristic $p$ if its prime field is isomorphic to
  $\mathbb{F}_p$ for some prime $p$.
\end{definition}

\begin{proposition}
  If $K$ is a finite field, then $|K| = p^n$ for some prime $p$ and some $n \geq
  1$.
\end{proposition}


\section{Maximal Ideals and Prime Ideals}

\begin{definition}[Maximal ideal]
  An ideal $I$ in a commutative ring $R$ is called a \textbf{maximal ideal} if
  $I$ is a proper ideal for which there is no proper ideal $J$ with $I
  \subsetneq J$.
\end{definition}

\begin{proposition}
  A proper ideal $I$ in a commutative ring $R$ is a maximal ideal if and only if
  $R/I$ is a field.
\end{proposition}

\begin{proposition}
  If $k$ is a field, then $I = (x_1 - a_1, \ldots, x_n - a_n)$ is a maximal
  ideal in $k[x_1,\ldots, x_n]$ whenever $a_1, \ldots, a_n \in k$.
\end{proposition}

\begin{theorem}
  Let $R$ be a commutative nonzero ring. For any proper ideal $I \subset R$,
  there exists a maximal ideal $J \subset R$, such that $I \subset J$.
\end{theorem}

\begin{definition}[Prime ideal]
  An ideal $I$ in a commutative ring $R$ is called \textbf{prime ideal} if $I$
  is a proper ideal such that $ab \in  I$ implies $a \in I$ or $b \in I$.
\end{definition}

\begin{proposition}
  If $I$ is a proper ideal in a commutative ring $R$, then $I$ is a prime ideal
  if and only if $R/I$ is a domain.
\end{proposition}

\begin{corollary}
  Every maximal ideal is a prime ideal.
\end{corollary}

\begin{definition}[Multiplication of ideals]
  If $I$ and $J$ are ideals in a commutative ring $R$, then
  %
  \begin{align*}
    IJ = \set{\text{all finite sums~} \sum_{l} a_l b_l: a_l \in I, b_l \in J}
  \end{align*}
\end{definition}

\begin{theorem}
  If $R$ is a nonzero commutative ring, then $R$ has a maximal ideal. Indeed,
  every proper ideal $U$ in $R$ is contained in a maximal ideal.
\end{theorem}

\begin{proposition}
  Let $P$ be a prime ideal in a commutative ring $R$. If $I$ and $J$ are ideals
  with $IJ \subseteq P$, then $I \subseteq P$ or $J \subseteq P$.
\end{proposition}

\begin{definition}[Coprime ideals]
  Let $R$ be a commutative ring and $I,J$ ideals in $R$. Then $I$ and $J$ are
  \textbf{coprime ideals} if $I + J = R$, i.e.\ there exists $a \in I$ and $b
  \in J$ such that $a + b = 1$.
\end{definition}

\begin{theorem}[Chinese Remainder Theorem]
  Let $I_1, \ldots, I_k$ be pairwise coprime ideals in a commutative ring $R$.
  Then the homomorphism
  %
  \begin{align*}
    \varphi: & R \to R/I_1 \times \cdots \times R/I_k \\
             & x \mapsto (x + I_1, \ldots , x+ I_k)
  \end{align*}
  %
  is surjective and $\ker\varphi = I_1 \cap \ldots \cap I_k$.
\end{theorem}


\section{From Arithmetic to Polynomials}

\begin{theorem}[Division Algorithm]
  If $k$ is a field and $f(x), g(x) \in k[x]$ with $f(x) \neq 0$, then there are
  unique polynomials $q(x), r(x) \in k[x]$ with
  %
  \begin{align*}
    g = qf + r
  \end{align*}
  %
  where either $r = 0$ or $\deg(r) < \deg(f)$.
\end{theorem}

\begin{corollary}
  Let $R$ be a commutative ring, and let $f(x) \in R[x]$ be a monic polynomial.
  If $g(x) \in R[x]$, then there exist $q(x) , r(x) \in R[x]$ with
  %
  \begin{align*}
    g(x) = q(x) f(x) + r(x),
  \end{align*}
  %
  where either $r(x) = 0$ or $\deg(r) < \deg(f)$. (Now $q,r$ are not unique!)
\end{corollary}

\begin{theorem}
  If $k$ is a field, then every ideal $I$ in $k[x]$ is a principal ideal; that
  is, there is $d \in I$ with $I = (d)$. Moreover, if $I \neq (0)$, then $d$ can
  be chosen to be monic.
\end{theorem}

\begin{definition}[Root of a polynomial]
  If $f(x) \in k[x]$, where $k$ is a field, then a \textbf{root} of $f$ in $k$
  is an element $a \in k $ with $f(a) = 0$.
\end{definition}

\begin{lemma}
  Let $f(x) \in k[x]$, where $k$ is a field, and let $u \in k$. Then there is
  $q(x) \in k[x]$ with
  %
  \begin{align*}
    f(x) = q(x) (x-u) + f(u)
  \end{align*}
\end{lemma}

\begin{definition}[Irreducible]
  An element $p$ in a domain $R$ is \textbf{irreducible} if $p$ is neither $0$
  nor a unit and in every factorization $p = uv$ in $R$, either $u$ or $v$ is a
  unit.
\end{definition}

\begin{theorem}[Reduction of Coefficients]
  Let $f(x) = a_0 + a_1 x + \dotsm + a_{n-1}x^{n-1} + x^n \in \Z[x]$ be monic
  and let $p$ be prime. If $\overline{f}(x) = \overline{a_0} + \overline{a_1} x
  + \dotsm \overline{a_{n-1}}x^{n-1} + x^n$ is irreducible in $\mathbb{F}_p$,
  then $f$ is irreducible in $\mathbb{Q}[x]$.
\end{theorem}

\begin{lemma}[Translation]
  Let $g(x) \in \Z[x]$. If there is $c \in \Z$ with $g(x + c)$ irreducible in
  $\Z[x]$, then $g$ is irreducible in $\mathbb{Q}[x]$.
\end{lemma}

\begin{theorem}[Eisenstein Criterion]
  Let $f(x) = a_0 + a_1 x + \dotsm + a_{n-1}x^{n-1} + a_n x^n \in \Z[x]$. If
  there is a prime $p$ with $p \mid a_i$ for all $i < n$ but $p  \nmid a_n$ and
  $p^2  \nmid a_0$, then $f$ is irreducible in $\mathbb{Q}[x]$.
\end{theorem}


\section{Euclidean Rings and Principal Ideal Domains*}

\begin{definition}[Greatest common divisor]
  If $a,b$ lie in a commutative ring $R$, then a \textbf{greatest common
  divisor} of $a,b$ is a common divisor $d \in R$ which is divisible by every
  common divisor; that is, if $c \mid a$ and $c \mid b$, then $c \mid d$.
\end{definition}

\begin{definition}[Euclidean ring, Degree function]
  A \textbf{euclidean ring} is a domain $R$ that is equipped with a function
  %
  \begin{align*}
    \partial: R \setminus \set{0} \to \N,
  \end{align*}
  %
  called a \textbf{degree function}, such that
  %
  \begin{enumerate}

    \item $\partial(f) \leq \partial(fg)$ for all $f,g \in R$ with $f,g \neq 0$;

    \item \textbf{Division Algorithm:} for all $f,g \in R$ with $f  \neq 0$,
      there exist $q,r \in R$ with
      %
      \begin{align*}
        g = qf + r,
      \end{align*}
      %
      where either $r = 0$ or $\partial(r) < \partial(f)$.

  \end{enumerate}
\end{definition}

\begin{theorem}
  Let $R$ be a euclidean ring.
  \begin{enumerate}
    \item Every ideal $I$ in $R$ is a principal ideal.

    \item Every pair $a,b \in R$ has a $\gcd$, say $d$, that is a linear
      combination of $a$ and $b$.

    \item \textbf{Euclid's Lemma}: If an irreducible element $p \in R$ divides a
      product $ab$, then either $p \mid a$ or $p \mid b$.

    \item \textbf{Unique Factorization}: If $a \in R$ and $a = p_1 \hdots p_m$,
      where the $p_i$ are irreducible elements, then this factorization is
      unique in the following sense: if $a = q_1 \hdots q_k$, where the $q_j$
      are irreducible, then $k = m$ and the $q$'s can be reindexed so that $p_i$
      and $q_i$ are associates for all $i$.
  \end{enumerate}
\end{theorem}


\begin{definition}[Principal ideal domain]
  A \textbf{principal ideal domain} (PID) is a domain $R$ in which every ideal
  is a principal ideal.
\end{definition}


\section{Unique Factorization Domains}

\begin{definition}[Unique factorization domain, Factorial ring]
  A domain $R$ is a \textbf{UFD} (unique factorization domain or factorial ring)
  if
  %
  \begin{enumerate}
    \item every $r \in R$, neither 0 nor a unit, is a product of irreducibles;

    \item if $p_1 \hdots p_m = q_1 \hdots q_n$, where all $p_i$ and $q_j$ are
      irreducible, then $m = n$ and there is a permutation $\sigma \in S_n$ with
      $p_i$ and $q_{\sigma(i)}$ associates for all $i$.
  \end{enumerate}
\end{definition}

\begin{proposition}
  Let $R$ be a domain in which every $r \in R$, neither 0 nor a unit, is a
  product of irreducibles. Then $R$ is a UFD if and only if $(p)$ is a prime
  ideal in $R$ for every irreducible element $p \in R$.
\end{proposition}

\begin{lemma}\mbox{}

  \begin{enumerate}
    \item If $R$ is a commutative ring and
      %
      \begin{align*}
        I_1 \subseteq I_2 \subseteq \ldots \subseteq I_n \subseteq I_{n+1}
        \subseteq \ldots
      \end{align*}
      %
      is an ascending chain of ideals in $R$, then $J = \bigcup_{n \geq 1} I_n$
      is an ideal in $R$.

    \item If $R$ is a PID, then it has no infinite strictly ascending chain of
      ideals
      %
      \begin{align*}
        I_1 \subsetneq I_2 \subsetneq \ldots \subsetneq I_n \subsetneq I_{n+1}
        \subsetneq \ldots
      \end{align*}

    \item If $R$ is a PID and $r \in R$ is neither 0 nor a unit, then $r$ is a
      product of irreducibles.
  \end{enumerate}
\end{lemma}

\begin{theorem}
  Every PID is a UFD.
\end{theorem}

\begin{proposition}\label{prop:existenceofgcdinufd}
  If $R$ is a UFD, then a $\gcd(a_1, \ldots, a_n)$ of any finite set of elements
  $a_1, \ldots, a_n$ in $R$ exists.
\end{proposition}

\begin{remark}
  Proposition~\ref{prop:existenceofgcdinufd} does not say that the $\gcd$ is a
  linear combination of the elements. In general, this is not true.
\end{remark}

\begin{definition}[Relatively prime]
  Elements $a_1, \ldots, a_n$ in a UFD $R$ are called \textbf{relatively prime}
  if their $\gcd$ is a unit.
\end{definition}

Recall, that if $R$ is a domain, then the units in $R[x]$ are the units in $R$.

\begin{definition}[Primitive]
  A polynomial $f(x) = a_n x^n + \cdots + a_1 x + a_0 \in R[x]$, where $R$ is a
  UFD, is called \textbf{primitive} if its coefficients are relatively prime;
  that is, the only common divisors of $a_n, \ldots, a_1, a_0$ are units.
\end{definition}

\begin{remark}
  If $R$ is a UFD, then every irreducible $p(x) \in R[x]$ of positive degree is
  primitive. Otherwise, there is an irreducible $q \in R$ with $p(x) = q g(x)$;
  Note that $\deg(q) = 0$. Since $p$ is irreducible, its only factors are units
  and associates; since $q$ is not a unit, it must be an associate of $p$. Units
  have degree 0 in $R[x]$. Hence, associates in $R[x]$ have the same degree. As
  $p$ has positive degree, $q$ cannot be an associate. Contradiction.
\end{remark}

\begin{lemma}[Gauss]
  If $R$ is a UFD and $f(x), g(x) \in R[x]$ are both primitive, then their
  product $fg$ is also primitive.
\end{lemma}

\begin{lemma}
  Let $R$ be a UFD, let $Q = \Frac(R)$ and let $f(x) \in Q[x]$ be nonzero.
  %
  \begin{enumerate}
    \item There is a factorization
      %
      \begin{align*}
      f(x) = c(f) f^{*}(x),
      \end{align*}
      %
      where $c(f) \in Q$ and $f^{*} \in R[x]$ is primitive. This factorization
      is unique up to multiplication with a unit.

    \item If $f(x), g(x) \in R[x]$ , then $c(fg), c(f)c(g)$ are associates in
      $R$ and $(fg)^{*}, f^{*}g^{*}$ are associates in $R[x]$.

    \item Let $f(x) \in Q[x]$ have a factorization $f = qg^{*}$, where $q \in Q$
      and $g^{*} \in R[x]$ is primitive. Then $f \in R[x]$ if and only if $q\in
      R$.

    \item Let $g^{*},f \in R[x]$. If $g^{*}$ is primitive and $g^{*} \mid bf$,
      where $b \in R$ and $b \neq 0$, then $g^{*} \mid f$.
  \end{enumerate}
\end{lemma}

\begin{definition}[Associated primitive content, Content]
  Let $R$ be UFD with $Q = \Frac(R)$. If $f(x) \in Q[x]$, there is a
  factorization $f = c(f) f^{*}$, where $c(f) \in Q$ and $f^{*} \in R[x]$ is
  primitive. We call $c(f)$ the \textbf{content} of $f$ and $f^{*}$ the
  \textbf{associated primitive polynomial}.
\end{definition}

\begin{theorem}[Gauss]
  If $R$ is a UFD, then $R[x]$ is also a UFD.
\end{theorem}

\begin{corollary}
  If $k$ is a field, then $k[x_1, \ldots, x_n]$ is a UFD.
\end{corollary}

\begin{corollary}
  If $k$ is a field, then $p = p(x_1, \ldots, x_n) \in k[x_1, \ldots, x_n]$ is
  irreducible if and only if $p$ generates a prime ideal in $k[x_1, \ldots,
  x_n]$.
\end{corollary}

\begin{corollary}[Gauss' Lemma]
  Let $R$ be a UFD, let $Q = \Frac(R)$, and let $f(x) \in R[x]$. If $f = GH$ in
  $Q[x]$, then there is a factorization
  %
  \begin{align*}
    f = gh \text{~in~} R[x],
  \end{align*}
  %
  where $\deg(g) = \deg(G)$ and $\deg(h) = \deg(H)$; in fact, $G$ is a constant
  multiple of $g$ and $H$ is a constant multiple of $h$. Therefore, if $f$ does
  not factor into polynomials of smaller degree in $R[x]$, then $f$ is
  irreducible in $Q[x]$.
\end{corollary}
