## Algebra I & II

***UPDATE: I am now done with this project, some proofs may be added or small
corrections made, but now that the exam catalogue has been released I will stop
working on this.***

### Reading

These are as set of lectures notes from the second year mathematics bachelor
course "Algebra" at the ETH Zürich, read by Prof. R. Pandharipande in the
Autumn/Summer semesters of 2018/2019.

The Alebra II lecture notes were written by myself with the help of fellow
students. Philipp Weder kindly submitted his excellent Algebra I summary to be
included in this project.

Special thanks to

- Philipp Weder
- Florian Held
- Fausto Bradke
- Wayne Zeng
- Emie Sun

### Disclaimer

I cannot guarantee completeness, nor correctness either due to time constraints
or knowledge gaps. These notes are mainly for my own understanding and a way to
share them along with the source in a practical way. In other words, please
don't rely on these being a complete and thorough script of the lectures,
instead see them as what they are: a set of non-exhaustive lecture notes.

Do not hesitate to send me an email if you discover any mistakes, ranging over
mathematical errors, spelling mistakes, grammatical or typesetting issues - I'm
open to any suggestions.

### Missing Proofs

- Section 6.2 Missing step in showing the splitting field is in fact the
  splitting field.
- Theorem 6.6 (Lagrange)
- Theorem 7.15 (if-direction) (Gauss-Wantzel)
- Theorem 11.4 (Existence of Galois extension over field with characteristic 0,
  containing any arbitrary field)

### Compiling yourself

TL;DR: `latexmk -pdf *tex && latexmk -c`

Previously, these notes were written in Markdown. As the code became more
complex, this became more difficult to maintain. Now they are written in LaTeX.
To compile the notes yourself, make sure you have LaTeX installed. Then call

`latexmk -pdf *tex`

in the base directory of this project. To clean up all auxiliary files except
the `pdf` after compilation, call

`latexmk -c`

Most distributions of LaTeX will have this compilation tool by default. Note in
Ubuntu related distros you may need to install `latexmk` as a separate package.
