% This preamble does a lot of the heavy lifting to define the beautiful
% typesetting of this script.

% Originally developed by Andreas Wieser and Prof. Manfred Einsiedler.

% Page Layout {{{

\documentclass[11pt,a4paper,oneside]{book}

\usepackage{verbatim}
\usepackage{comment}
\usepackage{url}
\usepackage{indentfirst}
\usepackage[T1]{fontenc}

% Figures
\usepackage[final]{graphicx}
\usepackage{caption}
\usepackage{subcaption}
% Contains the command \FloatBarrier, which prevents a float from moving above
% or below that barrier.
\usepackage{placeins}
\usepackage{float}

% }}}
% Whitespace and Rules {{{

% Set line spacing to 1.25
\usepackage{setspace}
\setstretch{1.2}

% Kill indent at first line of new paragraphs
\setlength{\parindent}{15pt}

% Force footnotes to the bottom of the page
\usepackage[bottom]{footmisc}

% Using this for title page
\newcommand{\HRule}{\rule{\linewidth}{0.5mm}}

% }}}
% Margin Settings {{{

\usepackage{vmargin}
\RequirePackage[utf8]{inputenc}
\setmarginsrb  {1.15in}      % Left
               {0.6in}       % Top
               {1.0in}       % Right
               {0.8in}       % Bottom
               {20pt}        % Header height
               {0.25in}      % Header separation
               {9pt}         % Footer height
               {0.3in}       % Footer separation

% }}}
% Text Body {{{

% Let height of page content vary from page to page (instead of stretching text
% body)
\raggedbottom

\usepackage{mdframed}
\newmdenv[
  topline=false,
  bottomline=false,
  rightline=false,
  skipabove=\topsep,
  skipbelow=\topsep,
  innertopmargin=0pt,
  innerbottommargin=0pt,
]{siderules}

% }}}
% Text Hyphenation/Breaks {{{

\doublehyphendemerits=10000       % No consecutive line hyphens.
\brokenpenalty=10000              % No broken words across columns/pages.
\widowpenalty=9999                % Almost no widows at bottom of page.
\clubpenalty=9999                 % Almost no orphans at top of page.
\interfootnotelinepenalty=9999    % Almost never break footnotes.
\hyphenation{Mengen-lehre}        % Customise hyphenation rule for this word

% }}}
% Headers and Footers {{{

\usepackage{fancyhdr}
% Default pagestyle {{{

% Fancy has a head- and footrule, see below
\pagestyle{fancy}
% Clear old settings for headers and footers
\fancyhf{}

% Only mark the name of section
% N.b: This will be the last one starting on current page
\renewcommand{\sectionmark}[1]{\markright{#1}}

% Clear old settings
\lhead{}
\rhead{}

% Set line strength for header and footer
\renewcommand{\headrulewidth}{0.6pt}
\renewcommand{\footrulewidth}{0.6pt}

\rfoot{\thepage}
\cfoot{\small \href{mailto:rrueger@ethz.ch}{rrueger@ethz.ch} - \href{http://git.ethz.ch/rrueger/algebra-notes}{git.ethz.ch/rrueger/algebra-notes}}
\lfoot{}

% }}}
% Custom Pagestyle Plain {{{

% Use same settings as prescribed in default pagestyle

\fancypagestyle{plain}{%
  \fancyhf{}                           % Clear old settings
  \fancyfoot[R]{\thepage}              % ...except the right footer
  \renewcommand{\headrulewidth}{0.6pt} % Set line strength for header
  \renewcommand{\footrulewidth}{0.6pt} % Set line strength for footer
}
% }}}

% }}}
% Packages {{{
\usepackage{amsmath,amsfonts,amssymb,amscd,amsthm,xspace}
\usepackage{enumerate,mathtools}
\usepackage{dsfont}
\usepackage{tikz}
\usetikzlibrary{calc,quotes,angles,decorations.pathmorphing,shapes}
\usetikzlibrary{matrix}

% Figures
\usepackage{import}
\usepackage{xifthen}
\usepackage{pdfpages}
\usepackage{transparent}
% }}}
% Math Environments {{{

% Bold title, italic body text
\theoremstyle{plain}
\newtheorem{theorem}{Theorem}[chapter]

\newtheorem*{theorem*}{Theorem}
\newtheorem{proposition}[theorem]{Proposition}
\newtheorem{lemma}[theorem]{Lemma}

\newtheorem*{fact}{Fact}
\newtheorem*{Theorem*}{Theorem}
\newtheorem*{proposition*}{Proposition*}
\newtheorem{corollary}[theorem]{Corollary}
\newtheorem{example}[theorem]{Example}
\newtheorem{exercise}[theorem]{Exercise}
\newtheorem*{exercise*}{Exercise}
\newtheorem{essexercise}[theorem]{Important Exercise}
\newtheorem{reminder}[theorem]{Reminder}

\theoremstyle{remark}
\newtheorem*{remark}{Remark}

% Bold title, Roman body text
\theoremstyle{definition}
\newtheorem{definition}[theorem]{Definition}

\newtheorem*{definition*}{Definition*}
\newtheorem*{idea}{Proof Concept}

% Just in case the style had changed
\theoremstyle{plain}
\newcommand{\thistheoremname}{}
\newtheorem*{genericthm}{\thistheoremname}

\numberwithin{equation}{chapter}
\numberwithin{figure}{chapter}
% }}}
% Define new operators {{{
% Sets {{{
\renewcommand{\subset}{\subseteq}
\renewcommand{\supset}{\supseteq}

% Set without conditions
\newcommand{\set}[1]{\left\lbrace #1 \right\rbrace}
% Set with conditions (vertical line)
\newcommand{\setc}[2]{\left\lbrace #1 \mid #2\right\rbrace}
% Set without conditions, sometimes useful if the braces are far apart but don't
% contain bigger symbols
\newcommand{\setb}[1]{\bigl\lbrace #1 \bigr\rbrace}
% Set with conditions (vertical line)
\newcommand{\setbc}[2]{\bigl\lbrace #1 \mid #2\bigr\rbrace}
% }}}
% Limit Shortcuts {{{
% Limes Superior
\newcommand{\lims}[1]{%
\mathchoice%
  {\underset{#1}{\overline{\lim}} }%
  {\overline{\lim}_{#1}\, }%
  {\overline{\lim}_{#1}\, }%
  {\overline{\lim}_{#1}\, }%
}

% Limes Inferior
\newcommand{\limi}[1]{%
\mathchoice%
  {\underset{#1}{\underline{\lim}} }%
  {\underline{\lim}_{#1}\, }%
  {\underline{\lim}_{#1}\, }%
  {\underline{\lim}_{#1}\, }%f
}

%}}}
% Trigonometric functions {{{
\DeclareMathOperator{\arsinh}{arsinh}
\DeclareMathOperator{\arcosh}{arcosh}
\DeclareMathOperator{\artanh}{artanh}
\DeclareMathOperator{\Si}{Si}
\DeclareMathOperator{\Ci}{Ci}
% }}}
% Analysis {{{
% Characteristic function
\DeclareMathOperator{\one}{\mathds{1}}
% Derivatives
\DeclareMathOperator{\de}{\thinspace{\rm{d}} }
\DeclareMathOperator{\De}{\thinspace{\rm{D}} }
\DeclareMathOperator{\divergence}{div}
\DeclareMathOperator{\rot}{rot}
\DeclareMathOperator{\laplace}{\Delta}
\DeclareMathOperator{\support}{supp}
\DeclareMathOperator{\graph}{\operatorname{graph}}
\DeclareMathOperator{\vol}{vol}
\DeclareMathOperator{\dvol}{\,\mathrm{dvol}}
\DeclareMathOperator{\re}{Re}
\DeclareMathOperator{\im}{Im}
% Imaginary unit
\DeclareMathOperator{\ii}{\mathrm{i}}
\DeclareMathOperator{\euler}{\mathrm{e}}
% Vector
\newcommand{\vc}[1]{\mathbf{#1}}
\DeclareMathOperator{\metric}{\mathrm{d}}
\DeclareMathOperator{\tangent}{\mathrm{T}}
\DeclareMathOperator{\boldpartial}{\pmb{\partial}}
\newcommand{\real}[1]{\overline{#1}}
% }}}
% Linear Algebra {{{

\DeclareMathOperator{\id}{id}
\DeclareMathOperator{\Tr}{Tr}
\DeclareMathOperator{\Mat}{Mat}
\DeclareMathOperator{\Hom}{Hom}
\DeclareMathOperator{\Aut}{Aut}
% Span
\newcommand{\spn}[1]{\ensuremath{\left\langle #1 \right\rangle}}

% }}}
% Algebra {{{
\DeclareMathOperator{\Char}{Char}
\DeclareMathOperator{\Obj}{Obj}
\DeclareMathOperator{\Orb}{Orb}
\DeclareMathOperator{\Stab}{Stab}
\DeclareMathOperator{\Mod}{Mod}
\DeclareMathOperator{\Gal}{Gal}
\DeclareMathOperator{\Frac}{Frac}
\DeclareMathOperator{\Sym}{Sym}
\DeclareMathOperator{\lcm}{lcm}
\DeclareMathOperator{\sgn}{sgn}
\DeclareMathOperator{\irr}{\mathrm{irr}}
\DeclareMathOperator{\complex}{\mathbb{C}}
\DeclareMathOperator{\ann}{\mathrm{ann \,}}
\DeclareMathOperator{\tor}{\mathrm{tor \,}}

% right-quotient
\newcommand\rquot[2]{
  \mathchoice%
      {\text{\raise0.4ex\hbox{$#1$}\big/\lower0.4ex\hbox{$#2$}} }%
      {#1/\text{\hspace{-0.3ex}\vspace{-0.5ex}$#2$}}%
      {#1\,/#2}%
      {#1\,/#2}%
}
% }}}
% Rings and Fields {{{

\DeclareMathOperator{\R}{\mathbb{R}}
\DeclareMathOperator{\Rn}{\mathbb{R}^n}
\DeclareMathOperator{\Z}{\mathbb{Z}}
\DeclareMathOperator{\Q}{\mathbb{Q}}
\DeclareMathOperator{\C}{\mathbb{C}}
\DeclareMathOperator{\E}{\mathbb{E}}
\DeclareMathOperator{\N}{\mathbb{N}}
\DeclareMathOperator{\F}{\mathbb{F}}
\DeclareMathOperator{\K}{\mathbb{K}}

%}}}
% Groups {{{

\newcommand{\T}{\mathbb{T}}
\DeclareMathOperator{\SO}{SO}
\DeclareMathOperator{\GL}{GL}
\DeclareMathOperator{\SL}{SL}

% }}}
% Calligraphic Letters {{{
\newcommand{\cA}{\mathcal{A}}
\newcommand{\cB}{\mathcal{B}}
\newcommand{\cC}{\mathcal{C}}
\newcommand{\cD}{\mathcal{D}}
\newcommand{\cE}{\mathcal{E}}
\newcommand{\cF}{\mathcal{F}}
\newcommand{\cG}{\mathcal{G}}
\newcommand{\cH}{\mathcal{H}}
\newcommand{\cI}{\mathcal{I}}
\newcommand{\cJ}{\mathcal{J}}
\newcommand{\cK}{\mathcal{K}}
\newcommand{\cL}{\mathcal{L}}
\newcommand{\cM}{\mathcal{M}}
\newcommand{\cN}{\mathcal{N}}
\newcommand{\cO}{\mathcal{O}}
\newcommand{\cP}{\mathcal{P}}
\newcommand{\cQ}{\mathcal{Q}}
\newcommand{\cR}{\mathcal{R}}
\newcommand{\cS}{\mathcal{S}}
\newcommand{\cT}{\mathcal{T}}
\newcommand{\cU}{\mathcal{U}}
\newcommand{\cV}{\mathcal{V}}
\newcommand{\cW}{\mathcal{W}}
\newcommand{\cX}{\mathcal{X}}
\newcommand{\cY}{\mathcal{Y}}
\newcommand{\cZ}{\mathcal{Z}}
% }}}
% }}}
% Table of Contents {{{
\usepackage{titlesec}
\usepackage[titles]{tocloft}

% Format section titles in TOC
\renewcommand\cftsecfont{\scshape}

% Format page numbers in TOC
\renewcommand\cftsecpagefont{\normalfont}

% Set name of TOC
\AtBeginDocument{\renewcommand\contentsname{Table of Contents}}
\usepackage[hidelinks]{hyperref}

% Color settings for hyperlink references
\hypersetup{%
  colorlinks,
  allcolors=black
}
% }}}

% vim:foldmethod=marker:foldlevel=0
